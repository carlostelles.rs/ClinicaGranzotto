<?php
require_once('../inc/page_start.php');
require_once('../inc/page_header.php');
require_once('../gerenciador/classes/class.Conexao.php');
require_once('../gerenciador/classes/class.HtmlBuilder.php');
require_once('../gerenciador/classes/class.News.php');

$oConexao = new Conexao();
$oHtml    = new HtmlBuilder();
$oNew     = new News(empty($_POST['sBusca']) ? '' : $_POST['sBusca'], true, 15);
$oNew->iPagina = empty($_POST['iPagina']) ? 1 : $_POST['iPagina'];
?>
  <section>
    <div class="container">
      <?php
        if (isset($_POST['iCodigo']) || !empty($_GET['q'])) {
          if (isset($_POST['iCodigo'])) {
            $oNew->setId($_POST['iCodigo']);
          } else {
            $oNew->setUrl($_GET['q']);
          }
          $oNew->consulta($oConexao);
      ?>
		
        <div class="features">	
          <div class="row">
            <div class="col-md-12">
              <div class="feature">
                <h2><?php echo $oNew->getTitulo(); ?></h2>
                <div class="colored-line text-center"></div>
                <p class="text-right"><i>Publicado em <b><?php echo $oNew->getData(); ?></b></i></p>
                <?php echo $oNew->getTexto(); ?>
                <br>
                <?php 
                if (isset($_POST['iCodigo'])) {
                  echo $oHtml->btnVoltar("Redirect.send('" . $_POST['sAction'] . "', '" . urldecode($_POST['sBackPage']) . "')"); 
                } else {
                  echo $oHtml->btnVoltar("Redirect.send('../news/', '')"); 
                }
                ?>
              </div>
              <br><br>
            </div>
          </div>
      	</div>

      <?php
        } else {
        	$oNew->consulta($oConexao);
      ?>
        <div class="content-header">
          <?php
            echo $oHtml->openForm('form-pesquisa', 'form-pesquisa', $_SERVER['PHP_SELF']);
            echo $oHtml->hidden('iPagina', 'iPagina', $oNew->iPagina);
          ?>
            <div class="header-section">
              <div class="row">
                <div class="col-sm-offset-3 col-sm-6 col-xs-12">
                  <?php echo $oHtml->input('', 'sBusca', 'sBusca', $oNew->getBusca(), 100, false, false, 'Digite sua pesquisa'); ?>
                </div>
                <div class="col-sm-1 col-xs-6 form-button">
                  <?php echo $oHtml->btnPesquisar(); ?>
                </div>
              </div>
            </div>
            <?php echo $oHtml->closeForm(); ?>
        </div>
      
        <div class="features">	
          <div class="row">
            <?php 
            if ($oNew->iLinhas) {
              foreach ($oNew->aResult as $key => $oResult) {
            ?>
            <div class="col-md-4">
              <a href="javascript:void(0)" class="new" onclick="Redirect.send($('#form-pesquisa').attr('action'), 'iCodigo=<?php echo $oResult->id; ?>');">
                <div class="feature" style="height: 400px;">
                  <h4 style="height: 60px; vertical-align: middle;"><?php echo $oResult->titulo; ?></h4>
                  <?php echo $oResult->resumo; ?>
                  <i class="pull-right">Publicado em <b><?php echo $oResult->data_publicacao; ?></b></i>
                </div>
              </a>
            </div>  
          <?php 
            }
          ?>
          </div>

          <div class="text-center">
            <?php echo $oHtml->paginacao('form-pesquisa', $oNew->aResult[0]->iTotalPag, $oNew->iPagina); ?>
          </div>

          <?php 
          } else {
            echo '<h3>Nenhum registro encontrado.</h3>';
          } 
          ?>
        </div>
      <?php
        }
      ?>
    </div>
  </section>

<?php
require_once('../inc/page_footer.php');
require_once('../inc/page_scripts.php');
require_once('../inc/page_end.php');
?>