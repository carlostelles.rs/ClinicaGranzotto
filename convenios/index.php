<?php
require_once('../inc/page_start.php');
require_once('../inc/page_header.php');
require_once('../gerenciador/classes/class.Conexao.php');
require_once('../gerenciador/classes/class.Convenios.php');

$oConexao = new Conexao();
$oConvenio = new Convenios();
$oConvenio->consulta($oConexao);
?>
  <section>
    <div class="container">
      <div class="brief text-left" style="padding-top: 15px;">
        <h2>Convênios</h2>
        <div class="colored-line pull-left"></div>
      </div>
      <div class="features">	
        <div class="row">
        <?php 
        foreach ($oConvenio->aResult as $key => $oResult) {
        ?>
          <div class="col-md-3" style="height: 200px;">
	          <div class="feature" align="center">
	            <?php if ($oResult->logo) { ?>
	            <img src="data:<?php echo $oResult->type; ?>;base64,<?php echo $oResult->logo; ?>" class="img-responsive" />
	            <?php } else { ?>
	            <h4 class="colored-text"><?php echo $oResult->descricao; ?></h4>
	            <?php } ?>
	          </div>
          </div>  
        <?php 
        }
        ?>
        </div>
      </div>
    </div>
  </section>

<?php
require_once('../inc/page_footer.php');
require_once('../inc/page_scripts.php');
require_once('../inc/page_end.php');
?>