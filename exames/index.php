<?php
require_once('../inc/page_start.php');
require_once('../inc/page_header.php');
require_once('../gerenciador/classes/class.Conexao.php');
require_once('../gerenciador/classes/class.Exames.php');

$oConexao = new Conexao();
$oExame = new Exames();
$oExame->consulta($oConexao);
?>
  <section>
    <div class="container">
      <div class="brief text-left" style="padding-top: 15px;">
        <h2>Exames</h2>
        <div class="colored-line pull-left"></div>
      </div>
      <div class="features">	
        <?php 
          if ($oExame->iLinhas) {
            foreach ($oExame->aResult as $i => $oResult) {
        ?>
        <div class="row">
          <div class="<?php echo empty($oResult->imagem) ? 'col-md-12' : 'col-md-8'; ?>">
            <h1 class="colored-text text-left"> 
              <?php echo $oResult->titulo; ?>
            </h1>
            <div class="text-justify">
              <?php echo $oResult->texto; ?>
            </div>
          </div>
          <?php 
            if ($oResult->imagem) {
          ?>
          <div class="col-md-4">
            <h1>&nbsp;</h1> 
            <div class="home-screenshot side-screenshot pull-left">
                <img src="data:<?php echo $oResult->type; ?>;base64,<?php echo $oResult->imagem; ?>" class="img-responsive" />
            </div>
          </div>
          <?php 
            }
          ?>
        </div>
        <br>
        <?php
            }
          }
        ?>  
      </div>
    </div>
  </section>

<?php
require_once('../inc/page_footer.php');
require_once('../inc/page_scripts.php');
require_once('../inc/page_end.php');
?>