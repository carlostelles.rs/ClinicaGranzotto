<?php
require_once('../inc/page_start.php');
require_once('../inc/page_header.php');
require_once('../gerenciador/classes/class.Conexao.php');
require_once('../gerenciador/classes/class.Banners.php');
require_once('../gerenciador/classes/class.News.php');

$oConexao = new Conexao();
$oBanner  = new Banners();
$oBanner->consulta($oConexao);
$oNew     = new News('', true, 5);
$oNew->setStatus('P');
$oNew->iPagina = 1;
$oNew->consulta($oConexao);
?>
<header>
  <div class="color-overlay">
    <div class="container">
      <div id="banners" class="owl-carousel owl-theme">
        <?php 
          if ($oBanner->iLinhas) {
            foreach ($oBanner->aResult as $i => $oResult) {
        ?>
        <div class="row">
          <?php 
            if ($oResult->imagem) {
          ?>
          <div class="col-md-6">
            <div class="home-screenshot side-screenshot" align="center">
                <img src="data:<?php echo $oResult->type; ?>;base64,<?php echo $oResult->imagem; ?>" class="img-responsive" />
            </div>
          </div>
          <?php 
            }
          ?>
          <div class="col-md-6 intro-section">
            <h1 class="intro text-left"> 
              <?php echo $oResult->titulo; ?>
            </h1>
            <div class="sub-heading text-left">
              <?php echo $oResult->texto; ?>
            </div>
          </div>
        </div>
        <?php
            }
          }
        ?>  
      </div>
    </div>
  </div>
</header>

  <section class="section1">
    <div class="container">
      <div id="noticias" class="owl-carousel owl-theme">
        <?php
          foreach ($oNew->aResult as $key => $oResult) {
        ?>
        <div class="row">

          <div class="col-md-12">
            <div class="brief text-justify">
              <h2><?php echo $oResult->titulo; ?></h2>
              <div class="colored-line pull-left"></div>
              <?php echo $oResult->resumo; ?>
              <a href="<?php echo $oResult->url; ?>">Leia mais</a>
            </div>
          </div>

        </div>
        <?php
          }
        ?>
      </div>
    </div>
  </section>

<?php
require_once('../inc/page_footer.php');
require_once('../inc/page_scripts.php');
require_once('../inc/page_end.php');
?>