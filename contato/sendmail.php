<?php
// Email Submit
// Note: filter_var() requires PHP >= 5.2.0
if ( isset($_POST['email']) && isset($_POST['email']) && isset($_POST['assunto']) && isset($_POST['mensagem']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
 
  // detect & prevent header injections
  $test = "/(content-type|bcc:|cc:|to:)/i";
  foreach ( $_POST as $key => $val ) {
    if ( preg_match( $test, $val ) ) {
      exit;
    }
  }

  $headers = 'From: ' . $_POST["nome"] . '<' . $_POST["email"] . '>' . "\r\n" .
      'Reply-To: ' . $_POST["email"] . "\r\n" .
      'X-Mailer: PHP/' . phpversion();

  $sMensagem  = 'Assunto: <b>'.$_POST['assunto'].'</b><br>';
  $sMensagem .= 'Telefone: <b>'.$_POST['telefone'].'</b><br>';
  $sMensagem .= 'Nome: <b>'.$_POST['nome'].'</b><br>';
  $sMensagem .= 'E-mail: <b>'.$_POST['email'].'</b><br>';
  $sMensagem .= 'Mensagem: <b>'.str_replace(chr(10), '<br>', $_POST['mensagem']).'</b><br>';
  if (mail( "contato@clinicagranzotto.com.br", 'MENSAGEM DE CONTATO ENVIADA PELO SITE', $sMensagem, $headers )) {
    echo true;
  } else {
    echo false;
  }
}
?>