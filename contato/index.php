<?php
require_once('../inc/page_start.php');
require_once('../inc/page_header.php');
?>

  <section>
    <div id="gmap" class="themed-background-muted" style="height: 300px;"></div>
  </section>

  <section class="contact-us">
    <div class="container">

      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h3 class="heading">Entre em Contato</h3>
          <div class="expanded-contact-form">
            <form class="contact-form" id="contato" role="form">

              <h6 class="success"><span class="olored-text icon_check"></span> Sua mensagem foi enviada com sucesso. </h6>
              <h6 class="error"><span class="colored-text icon_error-circle_alt"></span> O e-mail deve ser válido e a mensagem não pode ser muito curta. </h6>

              <div class="field-wrapper col-md-6">
                <input class="form-control input-box" id="nome" type="text" name="nome" placeholder="Nome">
              </div>

              <div class="field-wrapper col-md-6">
                <input class="form-control input-box" id="telefone" type="phone" name="telefone" placeholder="Telefone">
              </div>

              <div class="field-wrapper col-md-12">
                <input class="form-control input-box" id="email" type="text" name="email" placeholder="E-mail">
              </div>
              
              <div class="field-wrapper col-md-12">
                <input class="form-control input-box" id="assunto" type="text" name="assunto" placeholder="Assunto">
              </div>

              <div class="field-wrapper col-md-12">
                <textarea class="form-control textarea-box" id="mensagem" rows="7" name="mensagem" placeholder="Digite sua Mensagem"></textarea>
              </div>

              <button class="btn standard-button" type="submit" id="enviar" name="submit" data-style="expand-left">Enviar Mensagem</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php
require_once('../inc/page_footer.php');
require_once('../inc/page_scripts.php');
?>
  <!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps -->
  <script src="//maps.google.com/maps/api/js?sensor=true"></script>
  <script src="../js/gmaps.min.js"></script>
  <script>
    // Initialize map
    var contactMap = new GMaps({
      div: '#gmap',
      lat: -29.6775000,
      lng: -51.1163958,
      zoom: 16,
      disableDefaultUI: true,
      scrollwheel: false
    });

    contactMap.addMarkers([
        {
          lat: -29.6794128,
          lng: -51.1163958,
          infoWindow: {
            content: '<address align="left">' +
                      '<b>Clínica Granzotto</b><br>' +
                        'Avenida Dr. Maurício Cardoso, 833<br>' +
                        'Sala 707<br>' +
                        'Hamburgo Velho<br>' +
                        'Novo Hamburgo - RS<br>' +
                        'Brasil<br>' +
                        '<a href="tel:+555132390812">(51) 3239.0812</a>' +
                      '</address>'
          },
          animation: google.maps.Animation.DROP
        }
    ]);
  </script>
<?php
require_once('../inc/page_end.php');
?>