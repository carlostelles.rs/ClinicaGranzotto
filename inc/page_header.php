<!-- =========================
     HEADER   
============================== -->
  <div class="navigation-header">
    <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">

      <div class="container">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" style="margin-bottom: 30px;" data-toggle="collapse" data-target="#landx-navigation">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="javascript:void(0)"><img src="../images/logo.png" alt="" /></a>
        </div>

        <div class="navbar-collapse collapse" id="landx-navigation">
          <ul class="nav navbar-nav navbar-right main-navigation">
            <li><a href="../home">Home</a></li>
            <li>
              <div class="dropdown" style="margin-top: 8.5%;">
                <a href="#" id="areas" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Áreas de Atuação</a>
                <ul class="nav dropdown-menu" aria-labelledby="areas">
                  <li><a href="../garganta">Garganta</a></li>
                  <li><a href="../nariz">Nariz</a></li>
                  <li><a href="../ouvidos">Ouvidos</a></li>
                </ul>
              </div>
            </li>
            <li><a href="../exames">Exames</a></li>
            <li><a href="../convenios">Convênios</a></li>
            <li><a href="../news">Dicas e Notícias</a></li>
            <li><a href="../contato">Contato</a></li>
          </ul>
        </div>

      </div>

    </div>

  </div>