  <script src="../js/jquery.min.js"></script>
  <script>
    // makes sure the whole site is loaded
    jQuery(window).load(function() {
      "use strict";
      // will first fade out the loading animation
      jQuery(".status").fadeOut();
      // will fade out the whole DIV that covers the website.
      jQuery(".preloader").delay(1000).fadeOut("slow");
    });
  </script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/retina-1.1.0.min.js"></script>
  <script src="../js/smoothscroll.js"></script>
  <script src="../js/jquery.scrollTo.min.js"></script>
  <script src="../js/jquery.localScroll.min.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/nivo-lightbox.min.js"></script>
  <script src="../js/simple-expand.min.js"></script>
  <script src="../js/jquery.nav.js"></script>
  <script src="../js/jquery.fitvids.js"></script>
  <script src="../js/jquery.ajaxchimp.min.js"></script>
  <script src="../js/custom.js"></script>
  <script src="../gerenciador/js/util.js"></script>
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>