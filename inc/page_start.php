<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Clínica Granzotto">
  <meta name="keywords" content="Clínica Médica, Granzotto, Otorrino, Otorrinolaringologista">
  <meta name="author" content="4Up Sistemas">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- SITE TITLE -->
  <title>Clínica Granzotto</title>

  <!-- =========================
        FAV AND TOUCH ICONS  
  ============================== -->
  <link rel="icon" href="../images/favicon.ico">
  <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">

  <!-- =========================
       STYLESHEETS   
  ============================== -->
  <!-- BOOTSTRAP -->
  <link rel="stylesheet" href="../css/bootstrap.min.css">

  <!-- FONT ICONS -->
  <!-- IonIcons -->
  <link rel="stylesheet" href="../assets/ionicons/css/ionicons.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">  

  <!-- Elegant Icons -->
  <link rel="stylesheet" href="../assets/elegant-icons/style.css">
  <!--[if lte IE 7]><script src="../assets/elegant-icons/lte-ie7.js"></script><![endif]-->


  <!-- CAROUSEL AND LIGHTBOX -->
  <link rel="stylesheet" href="../css/owl.theme.css">
  <link rel="stylesheet" href="../css/owl.carousel.css">
  <link rel="stylesheet" href="../css/nivo-lightbox.css">
  <link rel="stylesheet" href="../css/nivo_themes/default/default.css">

  <!-- COLORS -->
  <link rel="stylesheet" href="../css/colors/blue.css">

  <!-- CUSTOM STYLESHEETS -->
  <link rel="stylesheet" href="../css/styles.css">

  <!-- RESPONSIVE FIXES -->
  <link rel="stylesheet" href="../css/responsive.css">

  <!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <!-- =========================
        PRE LOADER       
   ============================== -->
   <div class="preloader">
     <div class="status">&nbsp;</div>
   </div>