  <footer class="bgcolor-2">
    <div class="row">
      <div class="col-sm-offset-1 col-sm-3 col-xs-offset-2 col-xs-12">
        <div class="footer-logo">
          <img src="../images/logo@2.png" alt=""/>
        </div>
        <br>
        <address align="left">
            Avenida Dr. Maurício Cardoso, 833 - Sala 707<br>
            Novo Hamburgo - RS, Brasil<br>
            <a href="tel:+555132390812">(51) 3239.0812</a>
        </address> 
      </div>

      <div class="col-sm-4 col-xs-12">
        <!-- Facebook -->
        <div class="fb-like-box" data-href="https://www.facebook.com/DrGranzotto" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
      </div>
      
      <div class="col-sm-4 col-xs-12">
        <h6>Inscreva-se para receber nossas novidades!</h6>
        <form class="subscription-form mailchimp form-inline" role="form">
          <!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
          <h6 class="subscription-success"></h6>
          <h6 class="subscription-error"></h6>

          <!-- EMAIL INPUT BOX -->
          <input type="email" name="email" id="subscriber-email" placeholder="Seu E-mail" class="form-control input-box">
          <!-- SUBSCRIBE BUTTON -->
          <button type="submit" id="subscribe-button" class="btn standard-button">Inscrever</button>		
        </form>

        <div class="copyright">
          ©2015 Clínica Granzotto
        </div>	
      </div>
    </div>
  </footer>