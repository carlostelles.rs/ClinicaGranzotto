<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE CONVÊNIOS'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.Convenios.php'); 

$oConexao  = new Conexao($_SESSION['database']);
$oHtml     = new HtmlBuilder();
$oConvenio   = new Convenios();

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'excluir') {
  $oConvenio->setId($_POST['iCodigo']);
  $oConvenio->excluir($oConexao);
  if (!$oConvenio->iRetorno[0]) {
    $_POST['sAcao'] = 'editar';
  }
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'editar') {
  $oConvenio->setId($_POST['iCodigo']);
  $oConvenio->consulta($oConexao);
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'gravar') {
  $oConvenio->setId($_POST['iCodigo']);
  $oConvenio->setDescricao($_POST['descricao']);
  if ($_FILES['logo']) {
    if (($_FILES['logo']['size']/1024) <= 64) {
      $oConvenio->setType($_FILES['logo']['type']);
      $sCaminho =  '../temp/' . $_FILES["logo"]["name"];
      if (move_uploaded_file($_FILES['logo']['tmp_name'], $sCaminho)) {
  		$oArquivo = base64_encode(file_get_contents($sCaminho));
	    $oConvenio->setLogo($oArquivo);
	    unlink($sCaminho);
      } else {
      	$oConvenio->iRetorno[] = 0;
      	$oConvenio->sMensagem[] = 'Ocorreu algum erro ao realizar o upload do arquivo.';	
      }
    } else {
      $oConvenio->iRetorno[] = 0;
      $oConvenio->sMensagem[] = 'O arquivo não pode ser superior a 64KB. O arquivo atual possui '.($_FILES['logo']['size']/1024).'KB.';
    }
  }
  if (!$oConvenio->sMensagem[0]) {
    $oConvenio->gravar($oConexao);
    $oConvenio->consulta($oConexao);
  }
}

?>
<link rel="stylesheet" href="../css/custom.css">
<!-- Page content -->
<div id="page-content">   
  
  <div class="block full">

      <div class="block-title">
        <ul class="nav nav-tabs" data-toggle="tabs">
          <li class="active"><a href="#divisao-cadastro">Cadastro</a></li>
        </ul>
      </div>
        
      <div class="tab-content">
        <div class="tab-pane active" id="divisao-cadastro">
          <?php
            echo $oHtml->openForm('form-cadastro', 'form-cadastro', $_SERVER['PHP_SELF'], 'form-bordered', 'off', 'enctype="multipart/form-data"');
            echo $oHtml->hidden('sAcao', 'sAcao', 'gravar');
            echo $oHtml->hidden('sAction', 'sAction', $_POST['sAction']);
            echo $oHtml->hidden('sBackPage', 'sBackPage', $_POST['sBackPage']);
            echo $oHtml->hidden('iCodigo', 'iCodigo', $oConvenio->getId());
          ?>
          <div class="form-group">
            <div class="row mensage">
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->msgReturn($oConvenio->iRetorno, $oConvenio->sMensagem); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-xs-12">
                <?php echo $oHtml->input('Descrição', 'descricao', 'descricao', $oConvenio->getDescricao(), 100, true); ?>
              </div>
              <div class="col-sm-6 col-xs-12">
                <?php echo $oHtml->file('Logo', 'logo', 'logo'); ?>
              </div>
              <?php if ($oConvenio->getType()) { ?>
              <div class="col-sm-6 col-xs-12" align="center" style="padding-top: 10px;">
                  <div class="widget widget-content-full">
                    <img src="data:<?php echo $oConvenio->getType(); ?>;base64,<?php echo $oConvenio->getLogo(); ?>" name="imagem" class="img-responsive" />
                  </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <?php 
                  $oHtml->toolbarHistory  = true;
                  $oHtml->fncNew          = "Util.resetForm('form-cadastro'); $('img[name=imagem]').remove();";
                  $oHtml->fncDelete       = "Util.openDelete($('#iCodigo').val(), $('#descricao').val());";
                  $oHtml->fncHistory      = "Util.openHistory('" . criptoEncode('convenios') . "', $('#iCodigo').val());";
                  echo $oHtml->btnVoltar("Redirect.send('" . $_POST['sAction'] . "', '" . urldecode($_POST['sBackPage']) . "')");
                  echo $oHtml->toolbar('right', false);
                ?>
              </div>
            </div>
          </div>
          <?php echo $oHtml->closeForm(); ?>
        </div>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
require_once('../inc/page_footer.php');
require_once('../inc/template_scripts.php');
?>
<script src="../js/plugins/ckeditor/ckeditor.js"></script>
<?php
require_once('../inc/template_end.php'); 
?>