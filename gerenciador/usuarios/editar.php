<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE USUÁRIOS'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.Usuarios.php'); 

$oConexao  = new Conexao($_SESSION['database']);
$oHtml     = new HtmlBuilder();
$oUsuario   = new Usuarios();

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'excluir') {
  $oUsuario->setId($_POST['iCodigo']);
  $oUsuario->excluir($oConexao);
  if (!$oUsuario->iRetorno[0]) {
    $_POST['sAcao'] = 'editar';
  }
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'editar') {
  $oUsuario->setId($_POST['iCodigo']);
  $oUsuario->consulta($oConexao);
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'gravar') {
  
  if (!empty($_POST['senha'])) {
    if ($_POST['senha'] == $_POST['confirma_senha']) {
      $oUsuario->setSenha($_POST['senha']);
    } else {
      $oUsuario->iRetorno[] = 0;
      $oUsuario->sMensagem[] = 'A confirmação da senha não está igual a senha.';
    }
  }
  $oUsuario->setId($_POST['iCodigo']);
  $oUsuario->setNome($_POST['nome']);
  $oUsuario->setUsuario($_POST['usuario']);
  $oUsuario->setEmail($_POST['email']);
  if (!$oUsuario->sMensagem[0]) {
    $oUsuario->gravar($oConexao);
    $oUsuario->consulta($oConexao);
  }
}

?>
<link rel="stylesheet" href="../css/custom.css">
<!-- Page content -->
<div id="page-content">   
  
  <div class="block full">

      <div class="block-title">
        <ul class="nav nav-tabs" data-toggle="tabs">
          <li class="active"><a href="#divisao-cadastro">Cadastro</a></li>
        </ul>
      </div>
        
      <div class="tab-content">
        <div class="tab-pane active" id="divisao-cadastro">
          <?php
            echo $oHtml->openForm('form-cadastro', 'form-cadastro', $_SERVER['PHP_SELF'], 'form-bordered', 'off', 'enctype="multipart/form-data"');
            echo $oHtml->hidden('sAcao', 'sAcao', 'gravar');
            echo $oHtml->hidden('sAction', 'sAction', $_POST['sAction']);
            echo $oHtml->hidden('sBackPage', 'sBackPage', $_POST['sBackPage']);
            echo $oHtml->hidden('iCodigo', 'iCodigo', $oUsuario->getId());
          ?>
          <div class="form-group">
            <div class="row mensage">
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->msgReturn($oUsuario->iRetorno, $oUsuario->sMensagem); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-xs-12">
                <?php echo $oHtml->input('Nome', 'nome', 'nome', $oUsuario->getNome(), 50, true); ?>
              </div>
              <div class="col-sm-4 col-xs-12">
                <?php echo $oHtml->input('Usuário', 'usuario', 'usuario', $oUsuario->getUsuario(), 30, true); ?>
              </div>
              <div class="col-sm-4 col-xs-12">
                <?php echo $oHtml->input('E-mail', 'email', 'email', $oUsuario->getEmail(), 70, true); ?>
              </div>
              <div class="col-sm-4 col-xs-12">
                <?php echo $oHtml->password('Senha', 'senha', 'senha', 30, $oUsuario->getId() ? false : true); ?>
              </div>
              <div class="col-sm-4 col-xs-12">
                <?php echo $oHtml->password('Confirme a Senha', 'confirma_senha', 'confirma_senha', 30, $oUsuario->getId() ? false : true); ?>
              </div>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <?php 
                  $oHtml->toolbarHistory  = true;
                  $oHtml->fncNew          = "Util.resetForm('form-cadastro');";
                  $oHtml->fncDelete       = "Util.openDelete($('#iCodigo').val(), $('#usuario').val());";
                  $oHtml->fncHistory      = "Util.openHistory('" . criptoEncode('usuarios') . "', $('#iCodigo').val());";
                  echo $oHtml->btnVoltar("Redirect.send('" . $_POST['sAction'] . "', '" . urldecode($_POST['sBackPage']) . "')");
                  echo $oHtml->toolbar('right', false);
                ?>
              </div>
            </div>
          </div>
          <?php echo $oHtml->closeForm(); ?>
        </div>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
require_once('../inc/page_footer.php');
require_once('../inc/template_scripts.php');
?>
<script src="../js/plugins/ckeditor/ckeditor.js"></script>
<?php
require_once('../inc/template_end.php'); 
?>