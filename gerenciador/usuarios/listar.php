<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE USUÁRIOS'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.Usuarios.php'); 

$oConexao = new Conexao($_SESSION['database']);
$oHtml = new HtmlBuilder();
$oUsuario = new Usuarios(urldecode($_POST['sBusca']), true, 20);

if (isset($_POST['sActionDelete']) && $_POST['sActionDelete'] == 'excluir') {
  foreach ($_POST['iCodDelete'] as $iCodigo) {
    $oUsuario->setId($iCodigo);
    $oUsuario->excluir($oConexao);
  }
}  

$oUsuario->iPagina = empty($_POST['iPagina']) ? 1 : $_POST['iPagina'];
$oUsuario->consulta($oConexao);
?>
<!-- Page content -->
<div id="page-content">

    <div class="content-header">
      <?php
        echo $oHtml->openForm('form-pesquisa', 'form-pesquisa', $_SERVER['PHP_SELF']);
        echo $oHtml->hidden('iPagina', 'iPagina', $oUsuario->iPagina);
      ?>
        <div class="header-section">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <?php echo $oHtml->input('Busca', 'sBusca', 'sBusca', $oUsuario->getBusca(), 100, false, false, 'Digite sua pesquisa'); ?>
            </div>
            <div class="col-sm-2 col-xs-6 form-button">
              <?php echo $oHtml->btnPesquisar(); ?>
            </div>
          </div>
        </div>
        <?php echo $oHtml->closeForm(); ?>
    </div>

    <div class="block">
      <div class="tab-pane" id="pesquisa">
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <div class="widget">
              <?php 
                $oHtml->toolbarSave = false;
                $oHtml->toolbarEdit = true;
                $oHtml->toolbarGrid = true;
                $oHtml->fncEdit   = "Util.checkEdit('check-grid', 'editar.php');";
                $oHtml->fncNew    = "Redirect.send('editar.php');";
                $oHtml->fncDelete = "Util.checkDelete('form-pesquisa', 'check-grid');";
                echo $oHtml->toolbar();
              ?>
            </div>
          </div>
        </div>
        <div class="row mensage">
          <div class="col-sm-12 col-xs-12">
            <?php echo $oHtml->msgReturn($oUsuario->iRetorno, $oUsuario->sMensagem); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-xs-12">
              <div class="widget widget-list widget-content themed-background clearfix">
                <div class="col-sm-1 col-xs-2"><?php echo $oHtml->gridCheckAll(); ?></div>
                <div class="col-sm-4 col-xs-3"><b>Nome</b></div>
                <div class="col-sm-3 col-xs-3"><b>Usuário</b></div>
                <div class="col-sm-4 col-xs-4"><b>E-mail</b></div>
              </div>
          </div>
        </div>
        <?php
          if ($oUsuario->iLinhas > 0) {
            foreach ($oUsuario->aResult as $i => $oResult) {
              $sCorLinha = ($sCorLinha == 'themed-background-muted' ? 'themed-background-muted02' : 'themed-background-muted') 
        ?>
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <div class="widget widget-list widget-content <?php echo $sCorLinha; ?> clearfix">
                <div class="col-sm-1 col-xs-2"><?php echo $oHtml->gridCheck($oResult->id, $oResult->titulo, 'check-usuario-'.$oResult->id); ?></div>
                <div class="col-sm-4 col-xs-3"><strong><?php echo $oResult->nome; ?></strong></div>
                <div class="col-sm-3 col-xs-3"><?php echo $oResult->usuario; ?></div>
                <div class="col-sm-4 col-xs-4"><?php echo $oResult->email; ?></div>
              </div>
            </div>
          </div>
        <?php
            }
          } else {
        ?>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <p><strong>Nenhum registro encontrado.</strong></p>
              </div>
            </div>
        <?php
          }
          if ($oUsuario->bPaginacao && $oUsuario->iLinhas > 0) {
            echo $oHtml->paginacao('form-pesquisa', $oUsuario->aResult[0]->iTotalPag, $oUsuario->iPagina);
          }
        ?>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
  require_once('../inc/page_footer.php');
  require_once('../inc/template_scripts.php'); 
?>

<!-- Load and execute javascript code used only in this page -->
<script>
  $(function(){
    Redirect.backpage($('form#form-pesquisa').attr('action'), $('form#form-pesquisa').serialize());
  });
</script>

<?php require_once('../inc/template_end.php'); ?>