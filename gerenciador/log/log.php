<?php
require_once('../inc/config.php'); 
require_once('../classes/class.Conexao.php');
require_once('../classes/class.Log.php');

$sRegistro = $_POST['sRegistro'];
$sRotina = criptoDecode($_POST['sRotina']);

$oConexao = new Conexao($_SESSION['database']);
$oLog = new Log($sRegistro, $sRotina, $_SESSION['usuario']->id);
$oLog->consulta($oConexao);
echo json_encode($oLog->aResult);