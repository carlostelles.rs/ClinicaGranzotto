<?php
require_once('../inc/config.php');
require_once('../classes/class.Conexao.php'); 

$rotine = criptoDecode($_POST['rotine']);
$params = explode('||', $_POST['params']);

$oConexao  = new Conexao($_SESSION['database']);

try {
  $sSQL = $buildCombo['COMBO_SQL'][$rotine];
  $oConexao->open();
  $oConexao->prepare($sSQL);
  $oConexao->bindParam(':pBusca', '', 'string', 100);
  switch ($rotine) {
    case 'estado':
      $oConexao->bindParam(':pPais', $params[0], 'int', 20);
      break;
  }
  $oQuery = $oConexao->execute();
  $aReturn = array();
  if(!$oQuery) { 
    if($_SESSION['debug']){
      print($oConexao->getQuery());
    }
    $sErro = $oConexao->erro();
    $oConexao->close();
    die(print_r($sErro, true));
  } else {
    while ($row = mysqli_fetch_object($oQuery)) {
      $aItem = array();
      $aItem['value'] = $row->id;
      $aItem['text'] = $row->descricao;
      $aReturn[] = $aItem;
    }
  }
  $oConexao->close();
  echo json_encode($aReturn);
} catch (Exception $ex) {
  throw $ex;
}
