<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE BANNERS'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.Banners.php'); 

$oConexao = new Conexao($_SESSION['database']);
$oHtml = new HtmlBuilder();
$oBanner = new Banners(urldecode($_POST['sBusca']), true);

if (isset($_POST['sActionDelete']) && $_POST['sActionDelete'] == 'excluir') {
  foreach ($_POST['iCodDelete'] as $iCodigo) {
    $oBanner->setId($iCodigo);
    $oBanner->excluir($oConexao);
  }
}  

$oBanner->iPagina = empty($_POST['iPagina']) ? 1 : $_POST['iPagina'];
$oBanner->consulta($oConexao);
?>
<!-- Page content -->
<div id="page-content">

    <div class="content-header">
      <?php
        echo $oHtml->openForm('form-pesquisa', 'form-pesquisa', $_SERVER['PHP_SELF']);
        echo $oHtml->hidden('iPagina', 'iPagina', $oBanner->iPagina);
      ?>
        <div class="header-section">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <?php echo $oHtml->input('Busca', 'sBusca', 'sBusca', $oBanner->getBusca(), 100, false, false, 'Digite sua pesquisa'); ?>
            </div>
            <div class="col-sm-2 col-xs-6 form-button">
              <?php echo $oHtml->btnPesquisar(); ?>
            </div>
          </div>
        </div>
        <?php echo $oHtml->closeForm(); ?>
    </div>

    <div class="block">
      <div class="tab-pane" id="pesquisa">
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <div class="widget">
              <?php 
                $oHtml->toolbarSave = false;
                $oHtml->toolbarEdit = true;
                $oHtml->toolbarGrid = true;
                $oHtml->fncEdit   = "Util.checkEdit('check-grid', 'editar.php');";
                $oHtml->fncNew    = "Redirect.send('editar.php');";
                $oHtml->fncDelete = "Util.checkDelete('form-pesquisa', 'check-grid');";
                echo $oHtml->toolbar();
              ?>
            </div>
          </div>
        </div>
        <div class="row mensage">
          <div class="col-sm-12 col-xs-12">
            <?php echo $oHtml->msgReturn($oBanner->iRetorno, $oBanner->sMensagem); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-xs-12">
              <div class="widget widget-list widget-content themed-background clearfix">
                <div class="col-sm-1 col-xs-2"><?php echo $oHtml->gridCheckAll(); ?></div>
                <div class="col-sm-5 col-xs-10"><b>Título</b></div>
                <div class="col-sm-6 col-xs-12"><b>Texto</b></div>
              </div>
          </div>
        </div>
        <?php
          if ($oBanner->iLinhas > 0) {
            foreach ($oBanner->aResult as $i => $oResult) {
              $sCorLinha = ($sCorLinha == 'themed-background-muted' ? 'themed-background-muted02' : 'themed-background-muted') 
        ?>
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <div class="widget widget-list widget-content <?php echo $sCorLinha; ?> clearfix">
                <div class="col-sm-1 col-xs-2"><?php echo $oHtml->gridCheck($oResult->id, $oResult->titulo, 'check-banner-'.$oResult->id); ?></div>
                <div class="col-sm-5 col-xs-10"><strong><?php echo $oResult->titulo; ?></strong></div>
                <div class="col-sm-6 col-xs-12"><?php echo $oResult->texto; ?></div>
              </div>
            </div>
          </div>
        <?php
            }
          } else {
        ?>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <p><strong>Nenhum registro encontrado.</strong></p>
              </div>
            </div>
        <?php
          }
          if ($oBanner->bPaginacao && $oBanner->iLinhas > 0) {
            echo $oHtml->paginacao('form-pesquisa', $oBanner->aResult[0]->iTotalPag, $oBanner->iPagina);
          }
        ?>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
  require_once('../inc/page_footer.php');
  require_once('../inc/template_scripts.php'); 
?>

<!-- Load and execute javascript code used only in this page -->
<script>
  $(function(){
    Redirect.backpage($('form#form-pesquisa').attr('action'), $('form#form-pesquisa').serialize());
  });
</script>

<?php require_once('../inc/template_end.php'); ?>