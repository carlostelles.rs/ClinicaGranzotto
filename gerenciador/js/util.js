var Redirect = function () {
  
  var sUrl, oForm, sBackPage, sAction;
  
  var init = function () {
    $('body').append('<form name="form-redirect" method="post">');
    oForm = $('form[name="form-redirect"]');
    oForm.attr('action', sUrl);
  };
  
  var url = function (value) {
    sUrl = value;
  };
  
  var createInput = function (string) {
    if(string) {
      var temp = string.split('&');
      for (var i = 0; i < temp.length; i++) {
        var input = temp[i].split('=');
        $('<input type="hidden" name="' + input[0] + '" value="' + input[1] + '" />').appendTo(oForm);
      }
    }
    if(sBackPage) {
      $('<input type="hidden" name="sBackPage" value="' + sBackPage + '" />').appendTo(oForm);
    }
    if(sAction) {
      $('<input type="hidden" name="sAction" value="' + sAction + '" />').appendTo(oForm);
    }
  };
  
  var createBackPage = function (action, string) {
    sBackPage = encodeURIComponent(string);
    sAction = action;
  };
  
  var page = function (sForm, iPag) {
    $('form#' + sForm).find('input#iPagina').val(iPag);
    $('form#' + sForm).submit();
  };
  
  var submit = function () {
    oForm.submit();
  };
  
  return {
    send: function (action, inputs) {
      url(action);
      init();
      createInput(inputs);
      submit();
    },
    backpage: function (action, inputs) {
      createBackPage(action, inputs);
    },
    page: function (sForm, iPag) {
      page(sForm, iPag);
    }
  };
  
}();

var Util = function () {  
  
  var autoCloseMensage = function () {
    setTimeout("$('.mensage').fadeOut();", 4000);
  };
  
  var resetForm = function (form) {
    $('form#' + form).find('input:hidden').each(function () {
      if ($(this).attr('name') != 'sAcao'
       && $(this).attr('name') != 'sAction'
       && $(this).attr('name') != 'sBackPage') {
        $(this).val('');
      }
    });
    $('form#' + form).find('input:text').val('');
    $('form#' + form).find('select option[value=""]').attr({'selected':'selected'});
    $('form#' + form).find('select').trigger('chosen:updated');
    $('form#' + form).find('textarea').val('');
  };
  
  var erro = function (erro){
    var mensagem;
    if($.trim(erro.responseText) === ''){
      mensagem = erro.status + ' - ' + erro.statusText + '<br>Ocorreu algum erro no processamento! Tente novamente ou entre em contato com o Administrador do Sistema.';
    } else {
      mensagem = erro.responseText;
    }
    $.bootstrapGrowl('<h4><b>Erro</b></h4><p>' + mensagem + '</p>', {
      type: 'danger',
      delay: 5000,
      allow_dismiss: true,
      offset: {from: 'bottom', amount: 20}
    });
  };
  
  var openLog = function (rotina, value) {
    $.ajax({ 
      url: "../log/log.php",
      type:"post",
      dataType:"json",
      cache:false,
      data: {
        sRegistro: value,
        sRotina: rotina
      },
      success: function(data){
        var modal = $('#modal-log');
        var table = $('table#table-log');
        var tbody = $(table).find('tbody.conteudo');
        $(tbody).find('tr').remove();
        if (data.length > 0) {
          for (var i=0; i<data.length; i++) {
            var conteudo = '<tr>' +
                              '<td align="center">' + data[i].data + '</td>' +
                              '<td>' + replaceAll(data[i].observacao, '||', '<br>') + '</td>' +
                              '<td>' + '(' + data[i].usuario + ') ' + (!data[i].nome ? 'Usuário não encontrado' : data[i].nome) + '</td>' +
                           '</tr>';
            $(conteudo).appendTo(tbody);
          }
        } else {
          var conteudo = '<tr>' +
                            '<td colspan="3"><b>Não há registros para exibição.</b></td>' +
                         '</tr>';
          $(conteudo).appendTo(tbody);
        }
        $(modal).modal();
      },
      error: function(e){
        erro(e);
      }
    });
  };
  
  var openDelete = function (iValue, sText) {
    var modal = $('#modal-excluir');
    var form = $('#form-excluir');
    $(form).append('<input type="hidden" name="iCodigo" value="'+iValue+'" />');
    $(modal).find('div.conteudo').append('<b>(' + iValue + ') ' + sText + '</b><br>');
    $(modal).modal();
  };
  
  var checkDelete = function (sForm, name) {
    var aValue = new Array();
    var aText = new Array();
    $('input[name="' + name + '"]').each(function () {
      if ($(this).is(':checked')) {
        var iValue = $(this).val();
        var sText = $(this).attr('data-text');
        aValue.push(iValue);
        aText.push(sText);
      }
    });
    if (aValue.length > 0) {
      var form = $('form#' + sForm);
      var modal = $('#modal-excluir');
      $(form).append('<input type="hidden" name="sActionDelete" value="excluir" />');
      $(modal).find('#btnDelConfirm').attr('type', 'button');
      $(modal).find('#btnDelConfirm').attr('onclick', "$('form#" + sForm + "').submit();");
      for (var i = 0; i < aValue.length; i++) {
        $(form).append('<input type="hidden" name="iCodDelete[]" value="'+aValue[i]+'" />');
        $(modal).find('div.conteudo').append('<b>(' + aValue[i] + ') ' + aText[i] + '</b><br>');
      }
      $(modal).modal();
    }
    delete aValue, aText;
    console.log(aValue, aText);
  };
  
  var closeDelete = function () {
    $('#modal-excluir').on('hidden.bs.modal', function () {
        $('input[name="iCodDelete[]"]').remove();
        $('input[name="sActionDelete"]').remove();
        var modal = $('#modal-excluir');
        $(modal).find('div.conteudo').html('');
        $(modal).find('#btnDelConfirm').attr('type', 'submit');
        $(modal).find('#btnDelConfirm').removeAttr('onclick');
    });
  };
  
  var checkEdit = function (name, action) {
    $('input[name="' + name + '"]').each(function () {
      if ($(this).is(':checked')) {
        var iValue = $(this).val();
        Redirect.send(action, 'sAcao=editar&iCodigo=' + iValue);
      }
    });
  };
  
  var checkGrid = function (name, nameAll) {
    var iChecked = 0,
        bChecked,
        bCheckAll = true;
    $('input[name="' + name + '"]').each(function () {
      if ($(this).is(':checked')) {
        bChecked = 1;
        iChecked++;
      } else {
        bCheckAll = false;
      }
    });
    if (bChecked && iChecked > 0) {
      $('[data-multi="enabled"]').prop('disabled', false);
    } else {
      $('[data-multi="enabled"]').prop('disabled', true);
    }
    if (bChecked && iChecked === 1) {
      $('[data-multi="disabled"]').prop('disabled', false);
    } else {
      $('[data-multi="disabled"]').prop('disabled', true);
    }
    if (!bCheckAll) {
      $('input[name="' + nameAll + '"]').prop('checked', false);
    }
  };
  
  var checkGridAll = function (nameAll, nameItem) {
    if ($('input[name="' + nameAll + '"]').is(':checked')) {
      $('input[name="' + nameItem + '"]').prop('checked', true);
    } else {
      $('input[name="' + nameItem + '"]').prop('checked', false);
    }
    checkGrid(nameItem, nameAll);
  };
  
  var changeForm = function (form) {
    $('form#' + form).find(':text, select, textarea').change(function () {
      $('input[name=iPagina]').val('1');
    });
  };
  
  var replaceAll = function (string, token, newtoken) {
    while (string.indexOf(token) != '-1'){
      string = string.replace(token, newtoken);
    }
    return string;
  };
  
  var mask = function () {
    $('#cnpj, .cnpj').mask('99.999.999/9999-99');
    $('#cpf, .cpf').mask('999.999.999-99');
    $('#cep, .cep').mask('99.999-999');
    $('#telefone, #celular, .phone').mask('(99) 9999-9999#');
    $('#telefone, #celular, .phone').attr('maxlength','15');
    $('.money').mask('#.##0,00', {reverse: true}); 
  }
  
  return {
    init: function () {
      autoCloseMensage();
      closeDelete();
      changeForm('form-pesquisa');
      mask();
    },
    resetForm: function (form) {
      resetForm(form);
    },
    openHistory: function (rotina, value) {
      if (value > 0) {
        openLog(rotina, value);
      }
    }, 
    erro: function (e) {
      erro(e);
    },
    replace: function (string, token, newtoken) {
      replaceAll(string, token, newtoken);
    },
    openDelete: function (value, text) {
      if (value > 0) {
        openDelete(value, text);
      }
    },
    checkDelete: function (form, name) {
      checkDelete(form, name);
    },
    checkEdit: function (name, action) {
      checkEdit(name, action);
    },
    checkGridAll: function (all, item) {
      checkGridAll(all, item);
    },
    checkGrid: function (item, all) {
      checkGrid(item, all);
    }
  };
  
}();

var Combo = function () {  
  
  var buildComboAjax = function (element, rotine, params) {
    $.ajax({ 
      url: "../ajax/combo.php",
      type:"post",
      dataType:"json",
      cache:false,
      /*async:false,*/
      data: {
        rotine: rotine,
        params: params
      },
      success: function(data){
        $(element).find('option').each( function () {
          if (this.value) {
            $(this).remove();
          }
        });
        if (data.length > 0) {
          for (var i=0; i<data.length; i++) {
            $('<option value=" ' + data[i].value + '">' + data[i].text + '</option>').appendTo(element);
          }
        }
        $(element).trigger('chosen:updated');
      },
      error: function(e){
        Util.erro(e);
      }
    });
  };
  
  return {
    buildAjax: function (element, rotine, params) {
      buildComboAjax(element, rotine, params);
    }
  };
}();