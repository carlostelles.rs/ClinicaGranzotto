$(document).ready(function(){
    /* mascaras */
    $('#cnpj, .cnpj').mask('99.999.999/9999-99');
    $('#cpf, .cpf').mask('999.999.999-99');
    $('#cep, .cep').mask('99.999-999');
    $('#telefone, #celular, .phone').mask('(99) 9999-9999#');
    $('.money').mask('#.##0,00', {reverse: true}); 
    $('#telefone, #celular, .phone').attr('maxlength','15');
    
    /* fecha abas no carregamento */
    $('.fechado').hide();
    
    /* executa pesquisa ao carregar pagina */
    if( typeof pesquisa == 'function') { pesquisa(0); }
});

function diaPrimeiro(data){
    var mes = data.getMonth() + 1;
    if(mes < 10)
        mes = "0" + mes;
    var ano = data.getFullYear();
    return "01/" + mes + "/" + ano; 
}

function diaAtual(data){
    var dia = data.getDate();
    if(dia < 10)
        dia = "0" + dia;
    var mes = data.getMonth() + 1;
    if(mes < 10)
        mes = "0" + mes;
    var ano = data.getFullYear();
    return dia + "/" + mes + "/" + ano; 
}

function formataFloat(value){    
    if(value.match(","))
        value = value.replace(/,/g, '');
    value = parseFloat(value);
    return value;
}

function formataStringToFloat(value){
    if(value.match("."))
        value = value.replace('.', '');
    if(value.match(","))
        value = value.replace(',', '.');
    value = parseFloat(value);
    return value;
}

function formataFloatToString(value){
    var str;
    if(value.search("."))
        str = value.replace(/./g, '');
    if(str.search(","))
        str = str.replace(/,/g, '.');
    return parseFloat(str);
}

function fntRetornaMes(value) {
    var i = parseInt(value) - 1;
    var meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    return meses[i];
}

function mensagem(mensagem,icon,tempo,posicao){
    if(!tempo){
        tempo = 5000;
    }
    if(!posicao){
      posicao = 'bottom';
    }
    mensagem = replaceAll(mensagem, '[', '<b>');
    mensagem = replaceAll(mensagem, ']', '</b>');
    $.bootstrapGrowl(mensagem, {
        type: icon,
        delay: tempo,
        allow_dismiss: true,
        offset: {from: posicao, amount: 20}
    });
}

function validate(element){
    var form = $(element).find('input[required], select[required], textarea[required]'),
        validacao = true;
    $(form).each(function(){
        var div = $(this).parent().attr('class');
        div = div.match(/input-group/g);
        if(div !== null){
            if($.trim(this.value) === '' && $(this).parent().parent().css('display') !== 'none'){
                var campo = $(this).parent().parent().find('label').html();
                campo = campo.replace('*','');
                mensagem('<b>'+campo+'</b> é de preencimento obrigatório!','danger',3000);
                $(this).focus();
                validacao = false;
                return false;
            }
        } else {
            if($.trim(this.value) === '' && $(this).parent().css('display') !== 'none'){
                var campo = $(this).parent().find('label').html();
                campo = campo.replace('*','');
                mensagem('<b>'+campo+'</b> é de preencimento obrigatório!','danger',3000);
                $(this).focus();
                validacao = false;
                return false;
            }
        }
    });
    return validacao;
}

function validaPis(pis){
    var ftap="3298765432",
        total=0,
        resto=0,
        numPIS=0,
        strResto="";
	numPIS=pis;
    if(numPIS==="" || numPIS===null){
        return false;
    }
    for(i=0;i<=9;i++){
        resultado = (numPIS.slice(i,i+1))*(ftap.slice(i,i+1));
        total=total+resultado;
    }
    resto = (total % 11);
    if(resto != 0){
        resto=11-resto;
    }
    if(resto===10 || resto===11){
        strResto=resto+"";
        resto = strResto.slice(1,2);
    }
    if(resto!==(numPIS.slice(10,11))){
        return false;
    }
    return true;
}


function validaCnpj(cnpj) {
    cnpj = cnpj.replace(/[^\d]+/g,'');
    if(cnpj === ''){
        return false;
    }   
    if(cnpj.length !== 14){
        return false;
    }
    if(cnpj === "00000000000000" || 
       cnpj === "11111111111111" || 
       cnpj === "22222222222222" || 
       cnpj === "33333333333333" || 
       cnpj === "44444444444444" || 
       cnpj === "55555555555555" || 
       cnpj === "66666666666666" || 
       cnpj === "77777777777777" || 
       cnpj === "88888888888888" || 
       cnpj === "99999999999999"){
        return false;
    }
    tamanho = cnpj.length - 2;
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for(i = tamanho; i >= 1; i--){
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if(resultado != digitos.charAt(0)){
        return false;
    }
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for(i = tamanho; i >= 1; i--){
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1)){
        return false;
        return false;
    }   
    return true;
}

function validaCpf(strCPF) {
    strCPF = strCPF.replace(/[^\d]+/g,'');
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF === "00000000000"){
        return false;
    }
    for (var i = 1; i <= 9; i++){
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }
    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11)){
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(9, 10))){
        return false;
    }
    Soma = 0;
    for (var i = 1; i <= 10; i++){
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    }
    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11)){
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(10, 11))){
        return false;
    }
    return true;
}

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) != '-1'){
        string = string.replace(token, newtoken);
    }
    return string;
}

function limpar(form,find){
    var input = $(form).find(find);
    $(input).each(function(){
        $(this).val('');
    });
    $(form).find('select.select-chosen').trigger('chosen:updated');
}

function paginacao(pagina, total_registros, num_registros, metodo){

    var pagLocal = $('#pesquisa ul.pagination');
    
    var pagAtual = pagina + 1;
    var pagTotal = Math.ceil(total_registros/num_registros);
    var anterior = pagAtual - 2;
    var proxima = pagAtual;
    var tmpAtual;
    
    if(pagAtual == 1){
        tmpAtual = pagAtual;
    } else if (pagAtual == 2) {
        tmpAtual = pagAtual - 1;
    } else {
        tmpAtual = pagAtual - 2;
    }
    var j = 1;

    if (pagAtual > 1) {
        $(pagLocal).append('<li class="prev"><a href="javascript:' + metodo + '(0)" data-toggle="tooltip" title="Primeiro"><i class="fa fa-angle-double-left"></i></a></li>');
        $(pagLocal).append('<li class="prev"><a href="javascript:' + metodo + '(' + anterior + ')" data-toggle="tooltip" title="Anterior"><i class="fa fa-chevron-left"></i></a></li>');
    } else {
        $(pagLocal).append('<li class="prev disabled"><a href="javascript:void(0)"><i class="fa fa-angle-double-left"></i></a></li>');
        $(pagLocal).append('<li class="prev disabled"><a href="javascript:void(0)"><i class="fa fa-chevron-left"></i></a></li>');
    }

    for (var i = tmpAtual; i <= pagTotal; i++) {
        if (pagAtual == i) {
            $(pagLocal).append('<li class="active"><a href="javascript:' + metodo + '(' + (i - 1) + ')">' + i + '</a></li>');
        } else {
            $(pagLocal).append('<li><a href="javascript:' + metodo + '(' + (i - 1) + ')">' + i + '</a></li>');
        } 
        if (j == 5) {
            break;
        }
        j++;
    }
    
    if (pagAtual >= pagTotal) {
        $(pagLocal).append('<li class="next disabled"><a href="javascript:void(0)"><i class="fa fa-chevron-right"></i></a></li>');
        $(pagLocal).append('<li class="next disabled"><a href="javascript:void(0)"><i class="fa fa-angle-double-right"></i></a></li>');
    } else {
        $(pagLocal).append('<li class="next"><a href="javascript:' + metodo + '(' + proxima + ')" data-toggle="tooltip" title="Próximo"><i class="fa fa-chevron-right"></i></a></li>');
        $(pagLocal).append('<li class="next"><a href="javascript:' + metodo + '(' + (pagTotal - 1) + ')" data-toggle="tooltip" title="Último"><i class="fa fa-angle-double-right"></i></a></li>');
    }
    
    $(pagLocal).find('li a').tooltip('toggle').tooltip('hide');  
    
}

function pesquisaCep(element, value){
    if($.trim(value) !== ''){
        $.ajax({ 
            url: "ajax/endereco_ajax.php",
            type:"post",
            dataType:"json",
            cache:false,
            async:false,
            timeout:3000,
            data: {
                acao: "cep",
                cep: value
            },
            success: function(data){
                $(element).find('input#bairro').val(data[0].bairro);
                $(element).find('input#rua').val(data[0].rua);
                $(element).find('select#pais').val(data[0].pais).change();
                $(element).find('select#pais').trigger('chosen:updated');
                var estado = $(element).find('select#estado');
                var cidade = $(element).find('select#cidade');
                comboEstado(estado,data[0].pais,data[0].estado);
                comboCidade(cidade,data[0].estado,data[0].cidade);
                if($.trim(data[0].rua) !== ''){
                    $(element).find('input#numero').focus();
                } else {
                    $(element).find('input#rua').focus();
                }
            },
            error: function(erro){
                mensagemErro(erro);
            }
        });  
    }
}