
var Login = function() {

    return {
        init: function() {
            /* Login form - Initialize Validation */
            $('#form-login').validate({
                errorClass: 'help-block animation-slideUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'login-email': {
                        required: true
                    },
                    'login-password': {
                        required: true
                    }
                },
                messages: {
                    'login-email': 'Informe seu usuário ou e-mail',
                    'login-password': 'Informe sua senha'
                }
            });
        }
    };
}();