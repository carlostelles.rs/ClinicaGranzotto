var Log = function (){
  
    var modalLog, tableLog;
    
    var init = function () {
        modalLog = $('#modal-log');
        tableLog = $('#table-log tbody.conteudo');
    };
  
    var listar = function (registro, fonte) {
        $.ajax({ 
            url: "../ajax/log_ajax.php",
            type:"post",
            dataType:"json",
            cache:false,
            async: true,
            data: {
                registro: registro,
                fonte: fonte
            },
            success: function(data){
                $(tableLog).find('tr').remove();
                if(data.length > 0){
                    $('.modal').modal('hide');
                    for(var i=0; i<data.length; i++){
                        $(tableLog).append('<tr><td class="text-center">' + data[i].data + '</td><td>' + negrito(data[i].observacao) + '</td><td>' + data[i].usuario + '</td></tr>');
                    }
                    $(modalLog).modal();
                }
            },
            error: function(erro){
                mensagemErro(erro);
            }
        }); 
    };
    
    var negrito = function (a) {
        a = replaceAll(a, '[', '<b>');
        a = replaceAll(a, ']', '</b>');
        return a;
    };
    
    return {
        init: function () {
            init();
        },
        listar: function (registro, fonte) {
            listar(registro, fonte);
        }
    };
}();

$(function(){ Log.init(); });