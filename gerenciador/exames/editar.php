<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE EXAMES'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.Exames.php'); 

$oConexao  = new Conexao($_SESSION['database']);
$oHtml     = new HtmlBuilder();
$oExame   = new Exames();

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'excluir') {
  $oExame->setId($_POST['iCodigo']);
  $oExame->excluir($oConexao);
  if (!$oExame->iRetorno[0]) {
    $_POST['sAcao'] = 'editar';
  }
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'editar') {
  $oExame->setId($_POST['iCodigo']);
  $oExame->consulta($oConexao);
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'gravar') {
  $oExame->setId($_POST['iCodigo']);
  $oExame->setTitulo($_POST['titulo']);
  $oExame->setTexto($_POST['texto']);
  if ($_FILES['imagem']) {
    if (($_FILES['imagem']['size']/1024) <= 64) {
      $oExame->setType($_FILES['imagem']['type']);
      $sCaminho =  '../temp/' . $_FILES["imagem"]["name"];
      move_uploaded_file($_FILES['imagem']['tmp_name'], $sCaminho);
      $oArquivo = base64_encode(file_get_contents($sCaminho));
      $oExame->setImagem($oArquivo);
      unlink($sCaminho);
    } else {
      $oExame->iRetorno[] = 0;
      $oExame->sMensagem[] = 'O arquivo não pode ser superior a 64KB. O arquivo atual possui '.($_FILES['imagem']['size']/1024).'KB.';
    }
  }
  if (!$oExame->sMensagem[0]) {
    $oExame->gravar($oConexao);
    $oExame->consulta($oConexao);
  }
}

?>
<link rel="stylesheet" href="../css/custom.css">
<!-- Page content -->
<div id="page-content">   
  
  <div class="block full">

      <div class="block-title">
        <ul class="nav nav-tabs" data-toggle="tabs">
          <li class="active"><a href="#divisao-cadastro">Cadastro</a></li>
        </ul>
      </div>
        
      <div class="tab-content">
        <div class="tab-pane active" id="divisao-cadastro">
          <?php
            echo $oHtml->openForm('form-cadastro', 'form-cadastro', $_SERVER['PHP_SELF'], 'form-bordered', 'off', 'enctype="multipart/form-data"');
            echo $oHtml->hidden('sAcao', 'sAcao', 'gravar');
            echo $oHtml->hidden('sAction', 'sAction', $_POST['sAction']);
            echo $oHtml->hidden('sBackPage', 'sBackPage', $_POST['sBackPage']);
            echo $oHtml->hidden('iCodigo', 'iCodigo', $oExame->getId());
          ?>
          <div class="form-group">
            <div class="row mensage">
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->msgReturn($oExame->iRetorno, $oExame->sMensagem); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->input('Título', 'titulo', 'titulo', $oExame->getTitulo(), 50, false); ?>
              </div>
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->editor('Texto', 'texto', 'texto', $oExame->getTexto(), false, false, '', '', 'rows="5"'); ?>
              </div>
              <div class="col-sm-6 col-xs-12">
                <?php echo $oHtml->file('Imagem', 'imagem', 'imagem'); ?>
              </div>
              <?php if ($oExame->getType()) { ?>
              <div class="col-sm-6 col-xs-12" align="center" style="padding-top: 10px;">
                  <div class="widget widget-content-full">
                    <img src="data:<?php echo $oExame->getType(); ?>;base64,<?php echo $oExame->getImagem(); ?>" name="imagem" class="img-responsive" />
                  </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <?php 
                  $oHtml->toolbarHistory  = true;
                  $oHtml->fncNew          = "Util.resetForm('form-cadastro'); $('img[name=imagem]').remove(); $('.cke_button__newpage_icon').click();";
                  $oHtml->fncDelete       = "Util.openDelete($('#iCodigo').val(), $('#titulo').val());";
                  $oHtml->fncHistory      = "Util.openHistory('" . criptoEncode('exames') . "', $('#iCodigo').val());";
                  echo $oHtml->btnVoltar("Redirect.send('" . $_POST['sAction'] . "', '" . urldecode($_POST['sBackPage']) . "')");
                  echo $oHtml->toolbar('right', false);
                ?>
              </div>
            </div>
          </div>
          <?php echo $oHtml->closeForm(); ?>
        </div>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
require_once('../inc/page_footer.php');
require_once('../inc/template_scripts.php');
?>
<script src="../js/plugins/ckeditor/ckeditor.js"></script>
<?php
require_once('../inc/template_end.php'); 
?>