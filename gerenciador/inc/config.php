<?php
/**/
ini_set('display_errors',0);
ini_set('display_startup_erros',0);
error_reporting(E_ERROR);
/**/
require_once('util.php');
/* Template variables */
$template = array(
    'name'              => 'Gerenciador | Clinica Granzotto',
    'version'           => '1.0',
    'author'            => '4Up Sistemas',
    'robots'            => 'noindex, nofollow',
    'title'             => 'Gerenciador | Clinica Granzotto',
    'description'       => '',
    'page_preloader'    => false,
    'header_navbar'     => 'navbar-inverse',
    'header'            => 'navbar-fixed-top',
    'layout'            => '',
    'sidebar'           => 'sidebar-visible-lg-full',
    'cookies'           => 'enable-cookies',
    'header_link'       => '',
    'theme'             => '4upsistemas',
    'inc_sidebar'       => 'page_sidebar',
    'inc_sidebar_alt'   => '',
    'inc_header'        => 'page_header',
    'active_page'       => $_SERVER['PHP_SELF'],
    'url'               => ''
);

$aMenu = array(
  array(
    'name'  => 'Dashboard',
    'url'   => '../dashboard',
    'icon'  => 'fa fa-dashboard'
  ),
  array(
    'name'  => 'Banners',
    'icon'  => 'fa fa-television',
    'url'   => '../banners/listar.php',
  ),
  array(
    'name'  => 'Convênios',
    'icon'  => 'fa fa-university',
    'url'   => '../convenios/listar.php',
  ),
  array(
    'name'  => 'Dicas e Notícias',
    'icon'  => 'fa fa-newspaper-o',
    'url'   => '../news/listar.php',
  ),
  array(
    'name'  => 'Exames',
    'icon'  => 'fa fa-stethoscope',
    'url'   => '../exames/listar.php',
  ),
  array(
    'name'  => 'Usuários',
    'icon'  => 'fa fa-users',
    'url'   => '../usuarios/listar.php',
  ),
  array(
    'name'  => 'Sair',
    'url'   => '../logoff.php',
    'icon'  => 'fa fa-power-off'
  ),
);

$buildCombo = array(
    'STATUS_NEWS' => array(
        'E' => 'EM EDIÇÃO',
        'P' => 'PUBLICADO',
        'O' => 'OCULTO'
    )
); 