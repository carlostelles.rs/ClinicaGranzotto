<header class="navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><?php if ($template['header']) { echo ' '. $template['header']; } ?>">
    <ul class="nav navbar-nav-custom">
        <li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
            </a>
        </li>

        <?php if ($template['header_link']) { ?>
        <li class="hidden-xs animation-fadeInQuick">
            <a href="javascript:Redirect.send('<?php echo $_POST['sAction']; ?>', '<?php echo urldecode($_POST['sBackPage']); ?>')"><strong><?php echo $template['header_link']; ?></strong></a>
        </li>
        <?php } ?>
    </ul>
</header>