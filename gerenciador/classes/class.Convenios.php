<?php
/**
 * Classe Convenios
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class Convenios {
  private $sBusca;
  public $iLinhas;
  public $bPaginacao;
  public $iPagina;
  public $iRegistros;
  public $aResult = array();
  public $iRetorno = array();
  public $sMensagem = array();
  
  private $id;
  private $descricao;
  private $logo;
  private $type;
    
  function __construct($busca = '', $paginacao = false, $registros = null) {
    $this->sBusca = $busca;
    $this->bPaginacao = $paginacao;
    $this->iRegistros = $registros;
  }

  function setId($var) {
    $this->id = (int)$var;
  }

  function setDescricao($var) {
    $this->descricao = $var;
  }

  function setLogo($var) {
    $this->logo = $var;
  }

  function setType($var) {
    $this->type = $var;
  }

  function setBusca($var) {
    $this->sBusca = $var;
  }
  
  function getId() { return $this->id; }
  function getDescricao() { return $this->descricao; }
  function getLogo() { return $this->logo; }
  function getType() { return $this->type; }
  function getBusca() { return $this->sBusca; }
          
  function consulta(Conexao $oConexao) {
    try {
      $this->iPagina  = $this->bPaginacao ? $this->iPagina : 0;
      
      $sSQL = "call spConsultarConvenios (:pId, :pBusca, :pPagina, :pRegistros);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pId',        $this->id,         'int',    11);
      $oConexao->bindParam(':pBusca',     $this->sBusca,     'string', 50);
      $oConexao->bindParam(':pPagina',    $this->iPagina,    'int',    11);
      $oConexao->bindParam(':pRegistros', $this->iRegistros, 'int',    11);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          if (!empty($this->id)) {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->setId($row->id);
              $this->setDescricao($row->descricao);
              $this->setLogo($row->logo);
              $this->setType($row->type);
            }
          } else {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->aResult[] = $row;
            }
          }
        }               
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function gravar(Conexao $oConexao) {
    try {
      $sSQL = "call spGravarConvenios (:pUsuario, :pSession, :pId, :pDescricao, :pType, :pLogo);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',   $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',   session_id(),              'string',  32);
      $oConexao->bindParam(':pId',        $this->id,                 'int',     20);
      $oConexao->bindParam(':pDescricao', $this->descricao,          'string',  100);
      $oConexao->bindParam(':pType',      $this->type,               'string',  50);
      $oConexao->bindParam(':pLogo',      $this->logo,               'html');
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId($row->registro);
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function excluir(Conexao $oConexao) {
    try {
      $sSQL = "call spExcluirConvenios (:pUsuario, :pSession, :pId);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',     $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',     session_id(),              'string',  32);
      $oConexao->bindParam(':pId',          $this->id,                 'int',     20);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId('');
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
      
}
