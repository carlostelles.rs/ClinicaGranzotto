<?php
/**
 * Classe Log
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class Log {
  private $sBusca;
  public $iLinhas;
  public $bPaginacao;
  public $iPagina;
  public $aResult = array();
  public $iRetorno;
  public $sMensagem;
  
  private $id;
  private $registro;
  private $rotina;
  private $observacao;
  private $session;
  private $usuario;
  private $data;
    
  function __construct($registro, $rotina, $usuario, $bPaginacao = false) {
    $this->registro = $registro;
    $this->rotina = $rotina;
    $this->usuario = $usuario;
    $this->bPaginacao = $bPaginacao;
  }

  function consulta(Conexao $oConexao) {
    try {
      $this->iPagina  = $this->bPaginacao ? $this->iPagina : 0;
      
      $sSQL = "call spConsultarLog (:pRegistro, :pRotina, :pUsuario, :pPagina);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pRegistro', $this->registro, 'int',    20);
      $oConexao->bindParam(':pRotina',   $this->rotina,   'string', 50);
      $oConexao->bindParam(':pUsuario',  $this->usuario,  'int',    20);
      $oConexao->bindParam(':pPagina',   $this->iPagina,  'int',    11);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          while ($row = mysqli_fetch_object($oQuery)) {
            $this->aResult[] = $row;
          }
        }               
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
}