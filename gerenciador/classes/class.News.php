<?php
/**
 * Classe News
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class News {
  private $sBusca;
  public $iLinhas;
  public $bPaginacao;
  public $iPagina;
  public $iRegistros;
  public $aResult = array();
  public $iRetorno = array();
  public $sMensagem = array();
  
  private $id;
  private $titulo;
  private $resumo;
  private $texto;
  private $status;
  private $data;
  private $url;
    
  function __construct($busca = '', $paginacao = false, $registros = null) {
    $this->sBusca = $busca;
    $this->bPaginacao = $paginacao;
    $this->iRegistros = $registros;
  }

  function setId($var) {
    $this->id = (int)$var;
  }

  function setTitulo($var) {
    $this->titulo = $var;
  }

  function setTexto($var) {
    $this->texto = $var;
  }

  function setResumo($var) {
    $this->resumo = $var;
  }

  function setStatus($var) {
    $this->status = $var;
  }
  
  function setData($var) {
    $this->data = $var;
  }

  function setUrl($var) {
    $this->url = $var;
  }

  function setBusca($var) {
    $this->sBusca = $var;
  }
  
  function getId() { return $this->id; }
  function getTitulo() { return $this->titulo; }
  function getTexto() { return $this->texto; }
  function getResumo() { return $this->resumo; }
  function getStatus() { return $this->status; }
  function getData() { return $this->data; }
  function getUrl() { return $this->url; }
  function getBusca() { return $this->sBusca; }
          
  function consulta(Conexao $oConexao) {
    try {
      $this->iPagina = ($this->bPaginacao ? $this->iPagina : 0);
      
      $sSQL = "call spConsultarNews (:pId, :pStatus, :pBusca, :pUrl, :pPagina, :pRegistros);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pId',        $this->id,         'int',    11);
      $oConexao->bindParam(':pStatus',    $this->status,     'string', 1);
      $oConexao->bindParam(':pBusca',     $this->sBusca,     'string', 50);
      $oConexao->bindParam(':pUrl',       $this->url,        'string', 50);
      $oConexao->bindParam(':pPagina',    $this->iPagina,    'int',    11);
      $oConexao->bindParam(':pRegistros', $this->iRegistros, 'int',    11);
      //var_dump($oConexao=>getPrepare());
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          if (!empty($this->id) || !empty($this->url)) {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->setId($row->id);
              $this->setTitulo($row->titulo);
              $this->setTexto($row->texto);
              $this->setResumo($row->resumo);
              $this->setStatus($row->status);
              $this->setData($row->data_publicacao);
            }
          } else {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->aResult[] = $row;
            }
          }
        }               
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function gravar(Conexao $oConexao) {
    try {
      $sSQL = "call spGravarNews (:pUsuario, :pSession, :pId, :pTitulo, :pResumo, :pTexto, :pStatus, :pData);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',  $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',  session_id(),              'string',  32);
      $oConexao->bindParam(':pId',       $this->id,                 'int',     20);
      $oConexao->bindParam(':pTitulo',   $this->titulo,             'string',  100);
      $oConexao->bindParam(':pStatus',   $this->status,             'string',  1);
      $oConexao->bindParam(':pResumo',   $this->resumo,             'html');
      $oConexao->bindParam(':pTexto',    $this->texto,              'html');
      $oConexao->bindParam(':pData',     $this->data,               'date');
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId($row->registro);
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function excluir(Conexao $oConexao) {
    try {
      $sSQL = "call spExcluirNews (:pUsuario, :pSession, :pId);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',     $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',     session_id(),              'string',  32);
      $oConexao->bindParam(':pId',          $this->id,                 'int',     20);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId('');
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
      
}
