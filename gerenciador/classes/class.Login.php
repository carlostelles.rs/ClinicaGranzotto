<?php
/**
 * Classe Login
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class Login {
  public $iRetorno;
  public $sMensagem;
  
  public  $id;
  private $usuario;
  private $senha;
    
  function __construct($usuario = '', $senha = '') {
    $this->usuario = $usuario;
    $this->senha = $senha;
  }

  function login(Conexao $oConexao) {
    try {
      $this->iPagina  = $this->bPaginacao ? $this->iPagina : 0;
      
      $sSQL = "call spLogin (:pSession, :pUsuario, :pSenha);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pSession',  session_id(),    'string', 32);
      $oConexao->bindParam(':pUsuario',  $this->usuario,  'string', 70);
      $oConexao->bindParam(':pSenha',    $this->senha,    'string', 39);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno = $row->retorno;
          $this->sMensagem = $row->mensagem;
          $this->id = $row->registro;
        }               
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
}