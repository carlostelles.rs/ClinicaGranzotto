<?php

/**
 * BuilHtml é uma classe para construção de diversos objetos/elementos de marcação HTML.
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class HtmlBuilder {
    
    public $toolbarNew      = true;
    public $toolbarDelete   = true;
    public $toolbarSave     = true;
    public $toolbarHistory  = false;
    public $toolbarCancel   = false;
    public $toolbarEdit     = false;
    public $toolbarGrid     = false;
    
    public $fncNew          = '';
    public $fncDelete       = '';
    public $fncSave         = '';
    public $fncHistory      = '';
    public $fncCancel       = '';
    public $fncEdit         = '';
    
    public $btnGroupFunction = array();
    public $btnGroupColor    = array();
    public $btnGroupIcon     = array();
    public $btnGroupTitle    = array();
    public $btnGroupType     = array();
    public $btnGroupParam    = array();


    /**
    * Cria campo input 
    *
    * @author Carlos Telles
    * 
    * @param string $label Label/título do Campo (obrigatório)
    * @param string $name Name do Input (obrigatório)
    * @param string $id ID do Input (obrigatório)
    * @param string $value Valor do campo (default: vazio)
    * @param string $length Tamanho em caracteres do campo (default: vazio)
    * @param boolean $required Campo obrigatório (default: false)
    * @param boolean $readonly Somente leitura (default: false)
    * @param string $placeholder Dica/placeholder do campo (default: vazio)
    * @param string $class Class CSS adicionais ou personalizadas (default: vazio)
    * @param string $param Atributos adicionais (default: vazio)
    * @param string $style Estilos personalizados (default: vazio)
    * @return string Campo input em formato HTML
    */
   function input($label, $name, $id, $value = '', $length = 50, $required = false, $readonly = false, $placeholder = '', $class = '', $param = '', $style = '') {
       $rLabel = '';
       $rInput = '';
       $read = '';
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rInput = 'required';
       }
       if ($readonly) {
           $read = 'readonly';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $input = '<input type="text" name="'.$name.'" id="'.$id.'" value="'.$value.'" maxlength="'.$length.'" placeholder="'.$placeholder.'" class="form-control '.$class.'" style="'.$style.'" '.$rInput.' '.$read.' '.$param.'/>';
       return $label.$input;
   }
   
   function password($label, $name, $id, $length = 50, $required = false, $readonly = false, $placeholder = '', $class = '', $param = '', $style = '') {
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rInput = 'required';
       }
       if ($readonly) {
           $read = 'readonly';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $input = '<input type="password" name="'.$name.'" id="'.$id.'" maxlength="'.$length.'" placeholder="'.$placeholder.'" class="form-control '.$class.'" style="'.$style.'" '.$rInput.' '.$read.' '.$param.'/>';
       return $label.$input;
   }
   
   function file($label, $name, $id, $max = 1, $required = false, $placeholder = '', $class = '', $param = '', $style = '') {
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rInput = 'required';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $input = '<input type="file" name="'.$name.'" id="'.$id.'" max="'.$max.'" placeholder="'.$placeholder.'" class="form-control '.$class.'" style="'.$style.'" '.$rInput.' '.$read.' '.$param.'/>';
       return $label.$input;
   }
   
   /**
    * Cria campo date 
    *
    * @author Carlos Telles
    * 
    * @param string $label Label/título do Campo (obrigatório)
    * @param string $name Name do Input (obrigatório)
    * @param string $id ID do Input (obrigatório)
    * @param string $value Valor do campo (default: vazio)
    * @param boolean $required Campo obrigatório (default: false)
    * @param boolean $readonly Somente leitura (default: false)
    * @param string $style Estilos personalizados (default: vazio)
    * @return string Campo input em formato HTML
    */
   function date($label, $name, $id, $value = '', $required = false, $readonly = false, $style = '') {
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rInput = 'required';
       }
       if ($readonly) {
           $read = 'readonly';
       } else {
           $class = 'input-datepicker';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $input = '<input type="text" name="'.$name.'" id="'.$id.'" value="'.$value.'" data-date-format="dd/mm/yyyy" placeholder="dd/mm/aaaa" class="form-control '. $class .'" style="'.$style.'" '.$rInput.' '.$read.' />';
       return $label.$input;
   }

   /**
    * Cria campo hidden
    *
    * @author Carlos Telles
    * 
    * @param string $name Name do Input (obrigatório)
    * @param string $id ID do Input (obrigatório)
    * @param string $value Valor do campo (default: vazio)
    * @param string $param Atributos adicionais (default: vazio)
    * @return string Campo input hidden em formato HTML
    */
   function hidden($name, $id, $value = '', $param = '') {
       $input = '<input type="hidden" name="'.$name.'" id="'.$id.'" value="'.$value.'" '.$param.'/>';
       return $input;
   }

   /**
    * Cria campo select/combo 
    *
    * @author Carlos Telles
    * 
    * @param string $label Label/título do Campo (obrigatório)
    * @param string $name Name do Input (obrigatório)
    * @param string $id ID do Input (obrigatório)
    * @param array() $combo Array de duas posições para montar o combo. Primeira posição: value/id. Segunda posição: texto (obrigatório)
    * @param string $value Valor do campo (default: vazio)
    * @param string $default Option default (default: vazio)
    * @param boolean $required Campo obrigatório (default: false)
    * @param string $placeholder Dica/placeholder do campo (default: vazio)
    * @param string $class Class CSS adicionais ou personalizadas (default: vazio)
    * @param string $param Atributos adicionais (default: vazio)
    * @param string $style Estilos personalizados (default: vazio)
    * @return string Campo select/combo em formato HTML
    */
   function select($label, $name, $id, $combo, $value = '', $default = '', $required = false, $placeholder = '', $class = '', $param = '', $style = '') {
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rSelect = 'required';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $select = '<select name="'.$name.'" id="'.$id.'" class="select-chosen '.$class.'" style="'.$style.'" '.$rSelect.' '.$param.' data-placeholder="'.$placeholder.'">';
       if (!empty($default)){
           $select .= '<option value="">' . $default . '</option>';
       }
       foreach ($combo as $key => $text) {
           $selected = $key == $value ? 'selected' : '';
           $select .= '<option value="' . $key . '" ' . $selected . '>' . $text . '</option>';
       }
       $select .= '</select>';

       return $label.$select;
   }
   
    /**
    * Cria textarea 
    *
    * @author Carlos Telles
    * 
    * @param string $label Label/título do Campo (obrigatório)
    * @param string $name Name do Input (obrigatório)
    * @param string $id ID do Input (obrigatório)
    * @param string $value Valor do campo (default: vazio)
    * @param string $length Tamanho em caracteres do campo (default: vazio)
    * @param boolean $required Campo obrigatório (default: false)
    * @param boolean $readonly Somente leitura (default: false)
    * @param string $placeholder Dica/placeholder do campo (default: vazio)
    * @param string $class Class CSS adicionais ou personalizadas (default: vazio)
    * @param string $param Atributos adicionais (default: vazio)
    * @param string $style Estilos personalizados (default: vazio)
    * @return string Campo input em formato HTML
    */
   function textarea($label, $name, $id, $value = '', $length = 50, $required = false, $readonly = false, $placeholder = '', $class = '', $param = '', $style = '') {
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rInput = 'required';
       }
       if ($readonly) {
           $read = 'readonly';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $textarea = '<textarea name="'.$name.'" id="'.$id.'" maxlength="'.$length.'" placeholder="'.$placeholder.'" class="form-control '.$class.'" style="'.$style.'" '.$rInput.' '.$read.' '.$param.'>'.$value.'</textarea>';
       return $label.$textarea;
   }
   
   function editor($label, $name, $id, $value = '', $required = false, $readonly = false, $placeholder = '', $class = '', $param = '', $style = '') {
       if ($required) {
           $rLabel = '<span class="required">*&#160;</span>';
           $rInput = 'required';
       }
       if ($readonly) {
           $read = 'readonly';
       }
       $label = '<label>'.$rLabel.$label.'</label>';
       $textarea = '<textarea name="'.$name.'" id="'.$id.'" placeholder="'.$placeholder.'" class="ckeditor '.$class.'" style="'.$style.'" '.$rInput.' '.$read.' '.$param.'>'.$value.'</textarea>';
       return $label.$textarea;
   }
   
    /**
    * Cria switch
    *
    * @author Carlos Telles
    * 
    * @param string $label Label/título do Campo (obrigatório)
    * @param string $name Name do Input (obrigatório)
    * @param string $id ID do Input (obrigatório)
    * @param string $value Valor do campo (default: vazio)
    * @return string Campo input em formato HTML
    */
   function swicth($label, $name, $id, $value = '', $check = '') {
       if($check == $value) {
           $checked = 'checked';
       }
       $label = '<label>'.$label.'</label><br>';
       $swicth = '<label class="switch switch-primary"><input type="checkbox" name="'.$name.'" id="'.$id.'" value="'.$value.'" '.$checked.' /><span></span></label>';
       return $label.$swicth;
   }
   
   function checkbox($name, $id, $value, $check, $label = '', $param = '') {
     if ($check == $value) {
       $checked = 'checked';
     }
     $html = '<label class="csscheckbox csscheckbox-info" style="padding:0;"><input type="checkbox" name="'.$name.'" id="'.$id.'" value="'.$value.'" '.$checked.' '.$param.' /><span></span></label>';
     return $html;
   }
   
   function btnVoltar($funcao = '') {
       $button = '<button type="button" class="btn btn-effect-ripple btn-primary pull-left" onclick="'.$funcao.'"><i class="fa fa-arrow-circle-o-left"></i> <span class="hidden-xs">Voltar</span></button>';
       return $button;
   }
   
   function btnPesquisar($funcao = '') {
       $button = '<button type="submit" class="btn btn-effect-ripple btn-primary" onclick="'.$funcao.'"><i class="fa fa-search"></i> <span class="hidden-xs">Pesquisar</span></button>';
       return $button;
   }

   function toolbar($direction = 'left', $title = true) {  
    if($this->toolbarSave) {
      $this->btnGroupFunction[] = $this->fncSave;
      $this->btnGroupColor[]    = 'success';
      $this->btnGroupIcon[]     = 'fa-check';
      $this->btnGroupTitle[]    = 'Salvar';
      $this->btnGroupType[]     = 'submit';
      $this->btnGroupParam[]    = '';
    }
    if($this->toolbarEdit) {
      $this->btnGroupFunction[] = $this->fncEdit;
      $this->btnGroupColor[]    = 'success';
      $this->btnGroupIcon[]     = 'fa-pencil';
      $this->btnGroupTitle[]    = 'Editar';
      $this->btnGroupType[]     = 'button';
      $this->btnGroupParam[]    = 'data-multi="disabled" disabled';
    }
    if($this->toolbarDelete) {
      $this->btnGroupFunction[] = $this->fncDelete;
      $this->btnGroupColor[]    = 'danger';
      $this->btnGroupIcon[]     = 'fa-trash';
      $this->btnGroupTitle[]    = 'Excluir';
      $this->btnGroupType[]     = 'button';
      $this->btnGroupParam[]    = 'data-multi="enabled" '.($this->toolbarGrid ? 'disabled' : '');
    }
    if($this->toolbarNew) {
      $this->btnGroupFunction[] = $this->fncNew;
      $this->btnGroupColor[]    = 'primary';
      $this->btnGroupIcon[]     = 'fa-plus';
      $this->btnGroupTitle[]    = 'Novo';
      $this->btnGroupType[]     = 'button';
      $this->btnGroupParam[]    = '';
    }
    if($this->toolbarCancel) {
      $this->btnGroupFunction[] = $this->fncCancel;
      $this->btnGroupColor[]    = 'warning';
      $this->btnGroupIcon[]     = 'fa-ban';
      $this->btnGroupTitle[]    = 'Cancelar';
      $this->btnGroupType[]     = 'button';
      $this->btnGroupParam[]    = '';
    }
    if($this->toolbarHistory) {
      $this->btnGroupFunction[] = $this->fncHistory;
      $this->btnGroupColor[]    = 'default';
      $this->btnGroupIcon[]     = 'fa-clock-o';
      $this->btnGroupTitle[]    = 'Histórico';
      $this->btnGroupType[]     = 'button';
      $this->btnGroupParam[]    = '';
    }
    return $this->btnGroup($direction, $title);
   }
   
   function btnGroup($direction = 'left', $title = true) {
     $button = '<div class="btn-group pull-'.$direction.'" style="margin-bottom: 10px;">';
     for($i=0; $i<count($this->btnGroupFunction); $i++) {
       $button .= '<button type="'.$this->btnGroupType[$i].'" class="btn btn-effect-ripple btn-'.$this->btnGroupColor[$i].'" onclick="'.$this->btnGroupFunction[$i].'" '.($title ? 'data-toggle="tooltip" title="'.$this->btnGroupTitle[$i].'"' : '').' '.$this->btnGroupParam[$i].'><i class="fa '.$this->btnGroupIcon[$i].'"></i>'.(!$title ? ' '.$this->btnGroupTitle[$i] : '').'</button>';
     }
     $button .= '</div>';
     return $button;
   }
   
   function gridCheckAll($sId = 'check-grid-all') {
     return $this->checkbox('check-grid-all', $sId, 'ALL', '', '', 'onclick="Util.checkGridAll(\'check-grid-all\', \'check-grid\');"');
   }
   
   function gridCheck($sValue, $sText, $sId = 'check-grid-i') {
     return $this->checkbox('check-grid', $sId, $sValue, '', '', 'data-text="'.$sText.'" onclick="Util.checkGrid(\'check-grid\', \'check-grid-all\');"');
   }
   
   function paginacao($sForm, $iTotalPag, $iPagAtual) {
     $pagination = '<div class="text-center"><ul class="pagination">';
     if ($iPagAtual == 1) {
       $pagination .= '<li class="disabled"><a href="javascript:void(0)"><i class="fa fa-angle-double-left"></i></a></li>';
       $pagination .= '<li class="disabled"><a href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>';
     } else {        
       $pagination .= '<li><a href="javascript:Redirect.page(\''.$sForm.'\', '.(1).')"><i class="fa fa-angle-double-left"></i></a></li>';
       $pagination .= '<li><a href="javascript:Redirect.page(\''.$sForm.'\', '.($iPagAtual-1).')"><i class="fa fa-angle-left"></i></a></li>';
     }
     for ($i=1; $i<=$iTotalPag; $i++) {
       $active = '';
       if ($i == $iPagAtual) {
         $active = 'active';
       }
       if (($iPagAtual - 2) == $i || 
           ($iPagAtual - 1) == $i ||
            $iPagAtual == $i || 
           ($iPagAtual + 1) == $i ||
           ($iPagAtual + 2) == $i) {
          $pagination .= '<li class="'.$active.'"><a href="javascript:Redirect.page(\''.$sForm.'\', '.$i.')">' . $i . '</a></li>';
       }
     }
     if ($iPagAtual == $iTotalPag) {
       $pagination .= '<li class="disabled"><a href="javascript:void(0)"><i class="fa fa-angle-right"></i></a></li>';
       $pagination .= '<li class="disabled"><a href="javascript:void(0)"><i class="fa fa-angle-double-right"></i></a></li>';
     } else {
       $pagination .= '<li><a href="javascript:Redirect.page(\''.$sForm.'\', '.($iPagAtual+1).')"><i class="fa fa-angle-right"></i></a></li>';
       $pagination .= '<li><a href="javascript:Redirect.page(\''.$sForm.'\', '.($iTotalPag).')"><i class="fa fa-angle-double-right"></i></a></li>';
     }
     $pagination .= '</ul></div>';
     return $pagination;
   }

   /*
    * CRIAR:
    * - CAMPO TEXTAREA COM EDITOR
    * - CAMPOS RADIO
    */
   
   function openForm($sName, $sId, $sAction, $sClass = '', $sAutocomplete = 'off', $sParam = '') {
      $form = '<form name="'.$sName.'" id="'.$sId.'" action="'.$sAction.'" method="post" class="'.$sClass.'" autocomplete="'.$sAutocomplete.'" '.$sParam.'>';
      return $form;
   }
   
   function closeForm() {
      $form = '</form>';
      return $form;
   }
   
   function msgReturn($aRetorno, $aMsg) {
     $iDanger = 0;
     $iSuccess = 0;
     $iWarning = 0;
     for ($i=0; $i<count($aRetorno); $i++) {
       switch ($aRetorno[$i]) {
         case '0':
           $iDanger++;
           $sDanger = $aMsg[$i];
           break;
         case '1':
           $iSuccess++;
           $sSuccess = $aMsg[$i];
           break;
         case '2':
           $iWarning++;
           $sWarning = $aMsg[$i];
           break;
       }
     }
     if ($iDanger) {
       $sMsg .= $this->msg('danger', ($iDanger > 1 ? '<span class="badge">'.$iDanger.'</span> ' : '').$sDanger);
     }
     if ($iSuccess) {
       $sMsg .= $this->msg('success', ($iSuccess > 1 ? '<span class="badge">'.$iSuccess.'</span> ' : '').$sSuccess);
     }
     if ($iWarning) {
       $sMsg .= $this->msg('danger', ($iWarning > 1 ? '<span class="badge">'.$iWarning.'</span> ' : '').$sWarning);
     }
     return $sMsg;
   }
   
   function msg($sTipo, $sMensagem) {
      $html  = '<div class="alert alert-'.$sTipo.' alert-dismissable">';
      $sMensagem = str_replace('[', '<b>', $sMensagem);
      $sMensagem = str_replace(']', '</b>', $sMensagem);
      $html .= $sMensagem;
      $html .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>';
      $html .= '</div>';
      return $html;
   }
}
