<?php
/**
 * Classe Usuarios
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class Usuarios {
  private $sBusca;
  public $iLinhas;
  public $bPaginacao;
  public $iPagina;
  public $iRegistros;
  public $aResult = array();
  public $iRetorno = array();
  public $sMensagem = array();
  
  private $id;
  private $nome;
  private $usuario;
  private $email;
  private $senha;
    
  function __construct($busca = '', $paginacao = false, $registros = null) {
    $this->sBusca = $busca;
    $this->bPaginacao = $paginacao;
    $this->iRegistros = $registros;
  }

  function setId($var) {
    $this->id = (int)$var;
  }

  function setNome($var) {
    $this->nome = $var;
  }

  function setUsuario($var) {
    $this->usuario = $var;
  }

  function setEmail($var) {
    $this->email = $var;
  }
  
  function setSenha($var) {
    $this->senha = $var;
  }

  function setBusca($var) {
    $this->sBusca = $var;
  }
  
  function getId() { return $this->id; }
  function getNome() { return $this->nome; }
  function getUsuario() { return $this->usuario; }
  function getEmail() { return $this->email; }
  function getBusca() { return $this->sBusca; }
          
  function consulta(Conexao $oConexao) {
    try {
      $this->iPagina  = $this->bPaginacao ? $this->iPagina : 0;
      
      $sSQL = "call spConsultarUsuarios (:pId, :pBusca, :pPagina, :pRegistros);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pId',        $this->id,         'int',    11);
      $oConexao->bindParam(':pBusca',     $this->sBusca,     'string', 50);
      $oConexao->bindParam(':pPagina',    $this->iPagina,    'int',    11);
      $oConexao->bindParam(':pRegistros', $this->iRegistros, 'int',    11);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          if (!empty($this->id)) {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->setId($row->id);
              $this->setNome($row->nome);
              $this->setUsuario($row->usuario);
              $this->setEmail($row->email);
            }
          } else {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->aResult[] = $row;
            }
          }
        }               
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function gravar(Conexao $oConexao) {
    try {
      $sSQL = "call spGravarUsuarios (:pUsuario, :pSession, :pId, :pUser, :pNome, :pEmail, :pSenha);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',   $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',   session_id(),              'string',  32);
      $oConexao->bindParam(':pId',        $this->id,                 'int',     20);
      $oConexao->bindParam(':pUser',      $this->usuario,            'string',  30);
      $oConexao->bindParam(':pNome',      $this->nome,               'string',  50);
      $oConexao->bindParam(':pEmail',     $this->email,              'string',  70);
      $oConexao->bindParam(':pSenha',     $this->senha,              'string',  30);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId($row->registro);
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function excluir(Conexao $oConexao) {
    try {
      $sSQL = "call spExcluirUsuarios (:pUsuario, :pSession, :pId);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',     $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',     session_id(),              'string',  32);
      $oConexao->bindParam(':pId',          $this->id,                 'int',     20);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId('');
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
      
}
