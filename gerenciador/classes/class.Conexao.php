<?php
class Conexao {
  protected $oDB;
  protected $sHost = "webrepresentacoes.com";
  protected $iPort = 3306;
  protected $sUser = "sys_web";
  protected $sPswd = "jc7es8@dp9";
  protected $sDBname;
  private   $sQuery;
  private   $sPrepare;

  function __construct($sDBname = 'website_granzotto') {
    $this->sDBname = empty($sDBname) ? 'website_granzotto' : $sDBname;
  }

  #método que inicia conexao 
  function open(){        
    //header('Content-Type: text/html; charset=utf-8');
    $this->oDB = mysqli_connect($this->sHost,$this->sUser,$this->sPswd);
    if(!$this->oDB){
      die($this->expection("Não foi possível conectar ao Servidor!"));
    }
    if(!mysqli_select_db($this->oDB,$this->sDBname)){
      die($this->expection("Não foi possível conectar ao Banco de Dados!"));
    } else {
      mysqli_query($this->oDB, "SET NAMES 'utf8'");
      mysqli_query($this->oDB, 'SET character_set_connection=utf8');
      mysqli_query($this->oDB, 'SET character_set_client=utf8');
      mysqli_query($this->oDB, 'SET character_set_results=utf8');
      return $this->oDB;               
    }
  }

  //fecha a conexao com o banco de dados
  function close() {
    mysqli_close($this->oDB);
  }

  //metodo que executa acao no banco
  function query($sql){        
    $this->sQuery = $sql;
    $query = mysqli_query($this->oDB, $this->sQuery);
    return $query;
  }

  function execute() {
    $oQuery = $this->query($this->sPrepare);
    $this->sPrepare = '';
    return $oQuery;
  }

  function getQuery(){
    return $this->sQuery;
  }
  
  function getPrepare(){
    return $this->sPrepare;
  }

  //metodo que apresenta erro de banco
  function erro(){
    $query = mysqli_error($this->oDB);
    return $query;
  }

  //metodo que verifica numero de linhas da consulta
  function linhas($query){
    $num = mysqli_num_rows($query);
    return $num;
  }

  //metodo verifica status da conexao
  function statusCon(){
    if(!$this->oDB){
      echo "<h3>O sistema não está conectado à [$this->sDBname] em [$this->sHost].</h3>";
      exit;
    } else{
      echo "<h3>O sistema está conectado à [$this->sDBname] em [$this->sHost].</h3>";
    }
  }

  function prepare($sSQL) {
    if (!empty($sSQL)) {
      $this->sPrepare = $sSQL;
    } else {
      echo 'ERRO: String não informada!';
      return false;
    }
    return true;
  }

  /**
   * Metodo bindParam para evitar Injection
   * 
   * @author Carlos Telles <carlos@4upsistemas.com.br> 
   * 
   * @param int/string $sParam Posicao do parametro a ser substituido ou o proprio
   * @param string $sValor Valor do parametro
   * @param string $sTipo 
   * <table>
   *    <tr>
   *       <th>Parametro</th>
   *       <th>Descricao</th>
   *    </tr>
   *    <tr>
   *       <td>int</td>
   *       <td>Utilizado para <b>int</b> e <b>bigint</b></td>
   *    </tr>
   *    <tr>
   *       <td>date</td>
   *       <td>Utilizado para <b>date</b> <br> <sub>Formato que deve estar a string:</sub><br> dd/mm/yyyy</td>
   *    </tr>
   *    <tr>
   *       <td>datetime</td>
   *       <td>Utilizado para <b>datetime</b> ou <b>timestamp</b> <br> <sub>Formato que deve estar a string:</sub><br> dd/mm/yyyy hh:mm:ss</td>
   *    </tr>
   *    <tr>
   *       <td>boolean</td>
   *       <td>Utilizado para <b>bit</b> ou <b>tinyint</b> </td>
   *    </tr>
   *    <tr>
   *       <td>string</td>
   *       <td>Utilizado para <b>char</b> ou <b>varchar</b> </td>
   *    </tr>
   *    <tr>
   *       <td>html</td>
   *       <td>Utilizado para textos que possuam codigo de marcacao ou de execucao </td>
   *    </tr>
   * </table>
   * @param int $iTam Tamanho maximo permitido para o conteudo
   * 
   * @return boolean
   */
  function bindParam ($sParam, $sValor, $sTipo, $iTam = 0) {
    if (!empty($sParam)) {
      if ($iTam > 0) {
        $sValor = substr($sValor, 0, $iTam); 
      }

      # Remove palavras suspeitas de injection.
      $sValor = preg_replace(sql_regcase("/(\n|\r|%0a|%0d|Content-Type:|bcc:|to:|cc:|Autoreply:|from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "", $sValor);
      # Remove espacos vazios.
      $sValor = trim($sValor);        

      switch ($sTipo) {
        case 'int':
          if (!empty($sValor)) {
            $sValor = (int)$sValor;
          } else {
            $sValor = 'null';
          }
          break;
        case 'date':
          $aData = explode('/', $sValor);
          $sValor = $aData[2].'-'.$aData[1].'-'.$aData[0];
          $sValor = "'".$sValor."'";
          break;
        case 'datetime':
          $aData = explode(' ', $sValor);
          $aData[0][] = explode('/', $aData[0]);
          $sValor = $aData[0][2].'-'.$aData[0][1].'-'.$aData[0][0].' '.$aData[1];
          $sValor = "'".$sValor."'";
          break;
        case 'boolean':
          if ($sValor) {
            $sValor = 1;
          } else {
            $sValor = 0;
          }
          break;
        case 'string':
          $sValor = strip_tags($sValor);  # Remove tags HTML e PHP.
          $sValor = addslashes($sValor);  # Adiciona barras invertidas a uma string.
          $sValor = "'".$sValor."'";
          break;
        case 'html':
          $sValor = addslashes($sValor);  # Adiciona barras invertidas a uma string.
          $sValor = "'".$sValor."'";
          break;
      }
      $this->sPrepare = str_replace($sParam, $sValor, $this->sPrepare);
    } else {
      echo 'ERRO: Parametro não informado!';
      return false;
    }
    return true;
  }
  
  function expection($msg) {
    echo '<script>alert("'.$msg.'");</script>';
  }
  
}