<?php
/**
 * Classe Banners
 * @author Carlos Telles <carlos@4upsistemas.com.br> 
 * @copyright (c) 2015, 4Up Sistemas
 */
class Banners {
  private $sBusca;
  public $iLinhas;
  public $bPaginacao;
  public $iPagina;
  public $iRegistros;
  public $aResult = array();
  public $iRetorno = array();
  public $sMensagem = array();
  
  private $id;
  private $titulo;
  private $texto;
  private $imagem;
  private $type;
    
  function __construct($busca = '', $paginacao = false, $registros = null) {
    $this->sBusca = $busca;
    $this->bPaginacao = $paginacao;
    $this->iRegistros = $registros;
  }

  function setId($var) {
    $this->id = (int)$var;
  }

  function setTitulo($var) {
    $this->titulo = $var;
  }

  function setTexto($var) {
    $this->texto = $var;
  }

  function setImagem($var) {
    $this->imagem = $var;
  }

  function setType($var) {
    $this->type = $var;
  }

  function setBusca($var) {
    $this->sBusca = $var;
  }
  
  function getId() { return $this->id; }
  function getTitulo() { return $this->titulo; }
  function getTexto() { return $this->texto; }
  function getImagem() { return $this->imagem; }
  function getType() { return $this->type; }
  function getBusca() { return $this->sBusca; }
          
  function consulta(Conexao $oConexao) {
    try {
      $this->iPagina  = $this->bPaginacao ? $this->iPagina : 0;
      
      $sSQL = "call spConsultarBanners (:pId, :pBusca, :pPagina, :pRegistros);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pId',        $this->id,         'int',    11);
      $oConexao->bindParam(':pBusca',     $this->sBusca,     'string', 50);
      $oConexao->bindParam(':pPagina',    $this->iPagina,    'int',    11);
      $oConexao->bindParam(':pRegistros', $this->iRegistros, 'int',    11);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          if (!empty($this->id)) {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->setId($row->id);
              $this->setTitulo($row->titulo);
              $this->setTexto($row->texto);
              $this->setType($row->type);
              $this->setImagem($row->imagem);
            }
          } else {
            while ($row = mysqli_fetch_object($oQuery)) {
              $this->aResult[] = $row;
            }
          }
        }               
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function gravar(Conexao $oConexao) {
    try {
      $sSQL = "call spGravarBanners (:pUsuario, :pSession, :pId, :pTitulo, :pTexto, :pType, :pImagem);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',  $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',  session_id(),              'string',  32);
      $oConexao->bindParam(':pId',       $this->id,                 'int',     20);
      $oConexao->bindParam(':pTitulo',   $this->titulo,             'string',  50);
      $oConexao->bindParam(':pType',     $this->type,               'string',  70);
      $oConexao->bindParam(':pTexto',    $this->texto,              'html');
      $oConexao->bindParam(':pImagem',   $this->imagem,             'html');
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId($row->registro);
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
  
  function excluir(Conexao $oConexao) {
    try {
      $sSQL = "call spExcluirBanners (:pUsuario, :pSession, :pId);";
      $oConexao->open();
      $oConexao->prepare($sSQL);
      $oConexao->bindParam(':pUsuario',     $_SESSION['usuario']->id,  'int',     20);
      $oConexao->bindParam(':pSession',     session_id(),              'string',  32);
      $oConexao->bindParam(':pId',          $this->id,                 'int',     20);
      $oQuery = $oConexao->execute();
      if(!$oQuery) { 
        if($_SESSION['debug']){
          print($oConexao->getQuery());
        }
        $sErro = $oConexao->erro();
        $oConexao->close();
        die(print_r($sErro, true));
      } else {
        $this->iLinhas = $oQuery->num_rows;
        if($this->iLinhas > 0) {
          $row = mysqli_fetch_object($oQuery);
          $this->iRetorno[] = $row->retorno;
          $this->sMensagem[] = $row->mensagem;
          $this->setId('');
        }          
      }
      $oConexao->close();
    } catch (Exception $ex) {
      throw $ex;
    }
  }
      
}
