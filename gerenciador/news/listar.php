<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE DICAS E NOTÍCIAS'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.News.php'); 

$oConexao = new Conexao($_SESSION['database']);
$oHtml = new HtmlBuilder();
$oNew = new News(urldecode($_POST['sBusca']), true, 20);

if (isset($_POST['sActionDelete']) && $_POST['sActionDelete'] == 'excluir') {
  foreach ($_POST['iCodDelete'] as $iCodigo) {
    $oNew->setId($iCodigo);
    $oNew->excluir($oConexao);
  }
}  

$oNew->iPagina = empty($_POST['iPagina']) ? 1 : $_POST['iPagina'];
$oNew->consulta($oConexao);
?>
<!-- Page content -->
<div id="page-content">

    <div class="content-header">
      <?php
        echo $oHtml->openForm('form-pesquisa', 'form-pesquisa', $_SERVER['PHP_SELF']);
        echo $oHtml->hidden('iPagina', 'iPagina', $oNew->iPagina);
      ?>
        <div class="header-section">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <?php echo $oHtml->input('Busca', 'sBusca', 'sBusca', $oNew->getBusca(), 100, false, false, 'Digite sua pesquisa'); ?>
            </div>
            <div class="col-sm-2 col-xs-6 form-button">
              <?php echo $oHtml->btnPesquisar(); ?>
            </div>
          </div>
        </div>
        <?php echo $oHtml->closeForm(); ?>
    </div>

    <div class="block">
      <div class="tab-pane" id="pesquisa">
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <div class="widget">
              <?php 
                $oHtml->toolbarSave = false;
                $oHtml->toolbarEdit = true;
                $oHtml->toolbarGrid = true;
                $oHtml->fncEdit   = "Util.checkEdit('check-grid', 'editar.php');";
                $oHtml->fncNew    = "Redirect.send('editar.php');";
                $oHtml->fncDelete = "Util.checkDelete('form-pesquisa', 'check-grid');";
                echo $oHtml->toolbar();
              ?>
            </div>
          </div>
        </div>
        <div class="row mensage">
          <div class="col-sm-12 col-xs-12">
            <?php echo $oHtml->msgReturn($oNew->iRetorno, $oNew->sMensagem); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-xs-12">
              <div class="widget widget-list widget-content themed-background clearfix">
                <div class="col-sm-1 col-xs-2"><?php echo $oHtml->gridCheckAll(); ?></div>
                <div class="col-sm-2 col-xs-3"><b>Status</b></div>
                <div class="col-sm-4 col-xs-3"><b>Título</b></div>
                <div class="col-sm-2 col-xs-4"><b>Publicação</b></div>
                <div class="col-sm-2 col-xs-4"><b>Última Edição</b></div>
                <div class="col-sm-1 col-xs-2"></div>
              </div>
          </div>
        </div>
        <?php
          if ($oNew->iLinhas > 0) {
            foreach ($oNew->aResult as $i => $oResult) {
              $sCorLinha = ($sCorLinha == 'themed-background-muted' ? 'themed-background-muted02' : 'themed-background-muted') 
        ?>
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <div class="widget widget-list widget-content <?php echo $sCorLinha; ?> clearfix">
                <div class="col-sm-1 col-xs-2"><?php echo $oHtml->gridCheck($oResult->id, $oResult->titulo, 'check-new-'.$oResult->id); ?></div>
                <div class="col-sm-2 col-xs-3"><strong><?php echo $buildCombo['STATUS_NEWS'][$oResult->status]; ?></strong></div>
                <div class="col-sm-4 col-xs-3"><strong><?php echo $oResult->titulo; ?></strong></div>
                <div class="col-sm-2 col-xs-4"><?php echo $oResult->data_publicacao; ?></div>
                <div class="col-sm-2 col-xs-4"><?php echo $oResult->data_alteracao ? $oResult->data_alteracao : $oResult->data_criacao; ?></div>
                <div class="col-sm-1 col-xs-2"><a href="<?php echo $oResult->url; ?>" target="_blanck"><i class="fa fa-external-link"></i></a></div>
              </div>
            </div>
          </div>
        <?php
            }
          } else {
        ?>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <p><strong>Nenhum registro encontrado.</strong></p>
              </div>
            </div>
        <?php
          }
          if ($oNew->bPaginacao && $oNew->iLinhas > 0) {
            echo $oHtml->paginacao('form-pesquisa', $oNew->aResult[0]->iTotalPag, $oNew->iPagina);
          }
        ?>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
  require_once('../inc/page_footer.php');
  require_once('../inc/template_scripts.php'); 
?>

<!-- Load and execute javascript code used only in this page -->
<script>
  $(function(){
    Redirect.backpage($('form#form-pesquisa').attr('action'), $('form#form-pesquisa').serialize());
  });
</script>

<?php require_once('../inc/template_end.php'); ?>