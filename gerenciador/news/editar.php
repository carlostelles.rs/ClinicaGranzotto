<?php 
require_once('../inc/template_start.php');
$template['header_link'] = 'CADASTRO DE DICAS E NOTÍCIAS'; 
require_once('../inc/page_head.php'); 

require_once('../classes/class.Conexao.php'); 
require_once('../classes/class.HtmlBuilder.php'); 
require_once('../classes/class.News.php'); 

$oConexao  = new Conexao($_SESSION['database']);
$oHtml     = new HtmlBuilder();
$oNew   = new News();

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'excluir') {
  $oNew->setId($_POST['iCodigo']);
  $oNew->excluir($oConexao);
  if (!$oNew->iRetorno[0]) {
    $_POST['sAcao'] = 'editar';
  }
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'editar') {
  $oNew->setId($_POST['iCodigo']);
  $oNew->consulta($oConexao);
}   

if (isset($_POST['sAcao']) && $_POST['sAcao'] == 'gravar') {
  $oNew->setId($_POST['iCodigo']);
  $oNew->setTitulo($_POST['titulo']);
  $oNew->setResumo($_POST['resumo']);
  $oNew->setTexto($_POST['texto']);
  $oNew->setData($_POST['data']);
  $oNew->setStatus($_POST['status']);
  $oNew->gravar($oConexao);
  $oNew->consulta($oConexao);
}

?>
<link rel="stylesheet" href="../css/custom.css">
<!-- Page content -->
<div id="page-content">   
  
  <div class="block full">

      <div class="block-title">
        <ul class="nav nav-tabs" data-toggle="tabs">
          <li class="active"><a href="#divisao-cadastro">Cadastro</a></li>
        </ul>
      </div>
        
      <div class="tab-content">
        <div class="tab-pane active" id="divisao-cadastro">
          <?php
            echo $oHtml->openForm('form-cadastro', 'form-cadastro', $_SERVER['PHP_SELF'], 'form-bordered', 'off', 'enctype="multipart/form-data"');
            echo $oHtml->hidden('sAcao', 'sAcao', 'gravar');
            echo $oHtml->hidden('sAction', 'sAction', $_POST['sAction']);
            echo $oHtml->hidden('sBackPage', 'sBackPage', $_POST['sBackPage']);
            echo $oHtml->hidden('iCodigo', 'iCodigo', $oNew->getId());
          ?>
          <div class="form-group">
            <div class="row mensage">
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->msgReturn($oNew->iRetorno, $oNew->sMensagem); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-8 col-xs-12">
                <?php echo $oHtml->input('Título', 'titulo', 'titulo', $oNew->getTitulo(), 50, true); ?>
              </div>
              <div class="col-sm-2 col-xs-12">
                <?php echo $oHtml->date('Data de Publicação', 'data', 'data', $oNew->getData(), true); ?>
              </div>
              <div class="col-sm-2 col-xs-12">
                <?php echo $oHtml->select('Status', 'status', 'status', $buildCombo['STATUS_NEWS'], $oNew->getStatus(), '', true); ?>
              </div>
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->editor('Texto', 'texto', 'texto', $oNew->getTexto(), true, false, '', '', 'rows="10"'); ?>
              </div>
              <div class="col-sm-12 col-xs-12">
                <?php echo $oHtml->editor('Resumo', 'resumo', 'resumo', $oNew->getResumo(), true, false, '', '', 'rows="5"'); ?>
              </div>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <?php 
                  $oHtml->toolbarHistory  = true;
                  $oHtml->fncNew          = "Util.resetForm('form-cadastro'); $('.cke_button__newpage_icon').click();";
                  $oHtml->fncDelete       = "Util.openDelete($('#iCodigo').val(), $('#titulo').val());";
                  $oHtml->fncHistory      = "Util.openHistory('" . criptoEncode('news') . "', $('#iCodigo').val());";
                  echo $oHtml->btnVoltar("Redirect.send('" . $_POST['sAction'] . "', '" . urldecode($_POST['sBackPage']) . "')");
                  echo $oHtml->toolbar('right', false);
                ?>
              </div>
            </div>
          </div>
          <?php echo $oHtml->closeForm(); ?>
        </div>
      </div>
    </div>
</div>
<!-- END Page Content -->

<?php 
require_once('../inc/page_footer.php');
require_once('../inc/template_scripts.php');
?>
<script src="../js/plugins/ckeditor/ckeditor.js"></script>
<?php
require_once('../inc/template_end.php'); 
?>